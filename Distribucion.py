import numpy as np

def GaussLorentz(X, sig, a):
    X = np.array(X)
    #------------------ Distribución Gaussiana ------------------#
    FG = np.exp(- (X - a) ** 2 / (2 * sig ** 2)) / np.sqrt(2 * np.pi * sig ** 2)
    NG = 1 / sum(FG)
    fg = FG * NG

    #------------------ Distribución Lorenziana ------------------#
    FWHM = 2 * np.sqrt(2 * np.log(2)) * sig
    FL = 1 / (np.pi * FWHM * (1 + ((X - a) / FWHM) ** 2))
    NL = 1 / sum(FL)
    fl = FL * NL

    ff = 0.9 * fg + 0.1 * fl

    return ff

def GaussLorentz2(X, sig, a):
    X = np.array(X)
    #------------------ Distribución Gaussiana ------------------#
    FG = np.exp(- (X - a) ** 2 / (2 * sig ** 2)) / np.sqrt(2 * np.pi * sig ** 2)
    NG = 1 / sum(FG)
    fg = FG * NG

    #------------------ Distribución Lorenziana ------------------#
    FWHM = 2 * np.sqrt(2 * np.log(2)) * sig
    FL = 1 / (np.pi * FWHM * (1 + ((X - a) / FWHM) ** 2))
    NL = 1 / sum(FL)
    fl = FL * NL

    ff = 0.8 * fg + 0.2 * fl

    return ff
