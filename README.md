# Model-x-ray

Software open source for the quantification of samples by energy dispersive x-ray fluorescence by the fundamental parameter method using a continuos energy distribution model and an SDD detector.

This code can be modified accordin to the user's needs.

## Clone repository
```
git clone https://gitlab.com/paolo.tataje.b/model-x-ray.git
```

## Install dependencies

```
pip install -r requirements.txt
```

## Contact

Paolo G. Tataje:

ptataje@ipen.gob.pe
ptataje@ucm.es
paolo.tataje.b@uni.pe

## License
Open source - Free

