import numpy as np

def EnergiaDifraccion(Energia, Theta):

    Energia = float(Energia)
    Theta = float(Theta)
    Ang = np.cos(np.radians(Theta))

    Det = (1 / Energia) + ((1 - Ang) / 511) #------ 511 keV (mc**2)
    ED = round(1 / Det, 3)
    
    return ED