import numpy as np
import Interpolacion
import Minimos
import Distribucion

rhoSi=2.33 #g/cm3
                
ProSi = np.loadtxt('data\ProSi.txt') # Probability that a specific type of X-ray will be generated                     
ProSiE=ProSi[:,0]
ProSiRx=ProSi[:,1]

ProSiQx = np.loadtxt('data\DatSi.txt') # Energy jump probability and X-ray efficiency

SSi=ProSiQx[0,:]
WSi=ProSiQx[1,:]

uSi = np.loadtxt('data\Si.txt')


def EscapeSi(TElementRx, ProElementE, E):

    TElementRx = np.array(TElementRx)
    ProElementE = np.array(ProElementE)
    E = np.array(E)

    PosikaSi = np.array(np.where(E>1.74))
    PosikaSi = np.array(PosikaSi[0])
    PosikaSi = np.array(PosikaSi[0])

    PosikbSi = np.array(np.where(E>1.832))
    PosikbSi = np.array(PosikbSi[0])
    PosikbSi = np.array(PosikbSi[0])

    TRxEscapeka = np.zeros(len(ProElementE))
    TRxEscapekb = np.zeros(len(ProElementE))

    Posi2=np.zeros(len(ProElementE))
    for i in range(0,len(ProElementE)):
        Posi22=np.array(np.where(E>ProElementE[i]))
        Posic=np.array(Posi22[0])
        Posi2[i]=np.array(Posic[0])

    for i in range(0,len(ProElementE)):
        uSif1 = Interpolacion.Lineal(uSi[:,0]*10**3,uSi[:,3],E[int(Posi2[i])])*rhoSi
        uSit1 = Interpolacion.Lineal(uSi[:,0]*10**3,uSi[:,4],E[int(Posi2[i])])*rhoSi
        uSit2 = Interpolacion.Lineal(uSi[:,0]*10**3,uSi[:,4],E[int(PosikaSi)])*rhoSi
        uSit2b = Interpolacion.Lineal(uSi[:,0]*10**3,uSi[:,4],E[int(PosikbSi)])*rhoSi

        Ref = (WSi[0]*(SSi[0]-1)/SSi[0])
        if (ProElementE[i] > 1.839):
            TRxEscapeka[i] = round(Ref*ProSiRx[0]*TElementRx[i]*(uSif1/(2*uSit1))*(1-(uSit2/uSit1)*np.log(1+uSit1/uSit2)))
            TRxEscapekb[i] = round(Ref*ProSiRx[1]*TElementRx[i]*(uSif1/(2*uSit1))*(1-(uSit2b/uSit1)*np.log(1+uSit1/uSit2b)))
        else:
            TRxEscapeka[i] = 0
            TRxEscapekb[i] = 0


    return TRxEscapeka, TRxEscapekb

def EscapeModelado(TRxEscapeElementka, TRxEscapeElementkb, AnchoMediaAltura,
                   ProElementE, E):

    ProElementE = np.array(ProElementE)
    TRxEscapeElementka = np.array(TRxEscapeElementka)
    TRxEscapeElementkb = np.array(TRxEscapeElementkb)
    E = np.array(E)

    #------  For K-alpha of Si ------#
    AnchElement = Minimos.Cuadrados(AnchoMediaAltura[:,0],AnchoMediaAltura[:,1],1,ProElementE-1.74)
    SigElement = AnchElement/(2*np.sqrt(2*np.log(2)))

    FElementA = np.zeros([len(ProElementE), len(E)])
    FTElementA = np.zeros(len(E))    

    for i in range(0, len(ProElementE)):
        FElementA[i,:] = round(TRxEscapeElementka[i])*Distribucion.GaussLorentz(E, SigElement[i], ProElementE[i]-1.74)
        FTElementA = FTElementA + FElementA[i,:]

    #--------------------------------#
        
    #------  For K-betha of Si ------#        
    AnchElement = Minimos.Cuadrados(AnchoMediaAltura[:,0],AnchoMediaAltura[:,1],1,ProElementE-1.832)
    SigElement = AnchElement/(2*np.sqrt(2*np.log(2)))

    FElementB = np.zeros([len(ProElementE), len(E)])
    FTElementB = np.zeros(len(E))

    for i in range(0, len(ProElementE)):
        FElementB[i,:] = round(TRxEscapeElementkb[i])*Distribucion.GaussLorentz(E, SigElement[i], ProElementE[i]-1.832)
        FTElementB = FTElementB + FElementB[i,:]
        
    #--------------------------------#
    
    return FTElementA, FTElementB

def Suma(ProElementE, Tao, Tasa_Element):

    ProElement_Sum = []
    for i in range(len(ProElementE)):
        vector = []
        for j in range(i, len(ProElementE)):                        
            vector.append(ProElementE[i] + ProElementE[j])
        ProElement_Sum.append(vector)
    
    TkRx_Sum = []
    for i in range(len(ProElementE)):
        vector = []
        for j in range(i, len(ProElementE)):
            if(i == j):
                vector.append(0.0123 * Tao * Tasa_Element[i]**2 / 2)
            else:
                vector.append(0.0123 * Tao * Tasa_Element[i] * Tasa_Element[j])                    
        TkRx_Sum.append(vector)
    
    return ProElement_Sum, TkRx_Sum

def SumaModelado(ProElement_Sum, AnchoMediaAltura,
                 TkRx_Sum, E, Tiempo_Real):

    E = np.array(E)

    Ancho = []
    for i in range(len(ProElement_Sum)):
        vector = []
        for j in range(len(ProElement_Sum[i])):
            Vec = ProElement_Sum[i]
            AnchElement = Minimos.Cuadrados(AnchoMediaAltura[:,0], AnchoMediaAltura[:,1], 1, Vec[j])
            SigElement = AnchElement/(2*np.sqrt(2*np.log(2)))
            vector.append(SigElement)
        
        Ancho.append(vector)

    ProElement = [element for sublist in ProElement_Sum for element in sublist]
    SigElement = [element for sublist in Ancho for element in sublist]
    TkRxElement = [element for sublist in TkRx_Sum for element in sublist]

    FElement = np.zeros([len(ProElement), len(E)])
    FTElement = np.zeros(len(E))

    for i in range(0, len(ProElement)):
        FElement[i,:] = round(TkRxElement[i] * Tiempo_Real)*Distribucion.GaussLorentz(E, SigElement[i], ProElement[i])
        FTElement = FTElement + FElement[i,:]
    
    
    return FTElement



    #AnchElement = Minimos.Cuadrados(AnchoMediaAltura[:,0],AnchoMediaAltura[:,1],1,ProElementE)
    #SigElement = AnchElement/(2*np.sqrt(2*np.log(2)))


                   







        
        
