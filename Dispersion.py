import numpy as np

def ProductionAuL(Ek, CElement, rhoElement,         #'Ionization potential, Element Concentration, Element density
                  ProElement, ProElementQx,         #'Rx probability, Jump and Yield Probability
                  rhoSample, umaEF1, uT1,           #'Sample density, Coef. mass of EF
                  EAuL, IE, E, Posicion1kev):       #'Energys of X-ray gold, Intensity of Energy the X-ray gold, Energy and Position 1 keV
        
    Ek = float(Ek)
    CElement = float(CElement)
    rhoElement = float(rhoElement)
    rhoSample = float(rhoSample)
    umaEF1 =np.array(umaEF1)
    uT1 = np.array(uT1)
    Posicion1kev = int(Posicion1kev)
    EAuL = np.array(EAuL)
    IE = np.array(IE)
    E = np.array(E)

    ProElementE = ProElement[:,0]
    ProElementRx = ProElement[:,1]
    
    SElement = ProElementQx[0,:]
    WElement = ProElementQx[1,:]

    TkaElemRx = np.zeros(len(ProElementE))

    for i in range(0, len(EAuL)):
        if (Ek <= EAuL[i]):
            Posi = np.array(np.where(E > EAuL[i]))
            Posici=np.array(Posi[0])
            Posici1 = int(Posici[0])

            for j in range(0,len(ProElementE)):
            
                Posi = np.array(np.where(E > ProElementE[j]))
                Posici=np.array(Posi[0])
                Posici2 = int(Posici[0])

                TkaElemRx[j] = TkaElemRx[j]+CElement*IE[i]*(WElement[0]*(SElement[0]-1)/SElement[0])*ProElementRx[j]*(rhoElement/rhoSample)*umaEF1[Posici1-Posicion1kev]/(uT1[Posici1-Posicion1kev]+uT1[Posici2-Posicion1kev])
                    
            
            

    return TkaElemRx
