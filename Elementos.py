import numpy as np

def Encontrados(lista, Concentracion):
    Conc = np.array(Concentracion)

    value = ""
    Conce = ""
    aste = ""
    asterisco = "no"

    for i in range(0,len(Conc)):
        if (lista[i] != "H" and lista[i] != "He" and lista[i] != "Li" and
            lista[i] != "Be" and lista[i] != "B" and lista[i] != "C" and
            lista[i] != "N" and lista[i] != "O" and lista[i] != "F" and
            lista[i] != "Ne" and lista[i] != "Na" and lista[i] != "Mg"):

            if Conc[i] != 0:
                if Conc[i] > 0.000999999999:
                    value = value + lista[i] + ";"
                    Conce = Conce + str(Conc[i]*10**2) + ";"
                    aste = aste + ";"
                else:
                    value = value + lista[i] + ";"
                    Conce = Conce + str(Conc[i]*10**6) + ";"
                    aste = aste +"*;"
                    asterisco = "si"
                

    value = value.split(';')
    value = value[0:len(value)-1]

    Conce = Conce.split(';')
    Conce = Conce[0:len(Conce)-1]

    aste = aste.split(';')
    aste = aste[0:len(aste)-1]

    return value, Conce, aste, asterisco

def Incertidumbre(Concentracion, RxSimu, aste):
    Conc = np.zeros(len(Concentracion))
    RxSimu = np.array(RxSimu)

    for i in range(0, len(Conc)):
        if aste[i]=="*":
            Conc[i] = float(Concentracion[i])/10**6
        else:
            Conc[i] = float(Concentracion[i])/10**2


    Raiz = np.zeros(len(RxSimu))

    for i in range(0, len(Raiz)):
        Raiz[i] = np.sqrt(RxSimu[i])

    
    Inc = np.zeros(len(Raiz))

    for i in range(0, len(Inc)):
        Inc[i] = Raiz[i]*Conc[i]/RxSimu[i]
        if Conc[i] > 0.000999999999:
            Inc[i] = Inc[i]*10**2
        else:
            Inc[i] = Inc[i]*10**6


    return Inc












        

