#'-------------------------------------------------------------------------------------
#'This software has been developed by MSc. Paolo G. Tataje for the National University
#'of San Marcos
#'-------------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------------
#'Reservados todos los derechos. No se permite la reproducción total o parcial de
#'esta obra, ni su incorporación a un sistema informático, ni su transmisión en
#'cualquier forma o por cualquier medio (electrónico, mecánico, fotocopia, grabación
#'u otros) sin autorización previa y por escrito de los titulares del copyright. La
#'infracción de dichos derechos puede constituir un delito contra la propiedad
#'intelectual. copyright © Paolo G. Tataje., 2022
#'-------------------------------------------------------------------------------------


#'-------------------------------------------------------------------------------------
#'Algorithm for the quantification of elements using conventional technique. Based on
#'fundamental parameter method
#'-------------------------------------------------------------------------------------

#'-------------------------------------------------------------------------------------
#'--Code            :   011221
#'--Date            :   01/12/21
#'--User            :   Paolo G. Tataje
#'--Description     :   Place the icon
#'--Status          :   Complete 1st level
#'-------------------------------------------------------------------------------------
import sys
import numpy as np
import math as mt
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
from matplotlib.backends.backend_qt5agg import FigureCanvas
from matplotlib.figure import Figure
from pylab import *
import pyqtgraph as pg
import Minimos
import Interpolacion
import Distribucion
import Produccion as Prd
import Dispersion as Dp
import Inelastica as In
import Elementos
import PicosDe

import openpyxl
from openpyxl.styles import Border, Side

from PyQt5 import QtCore, QtGui, QtWidgets, uic
from PyQt5.QtGui import *
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

from reportlab.pdfbase.pdfmetrics import registerFont
from reportlab.lib.pagesizes import A4
from reportlab.lib import colors
from reportlab.lib.colors import black
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.pdfgen import canvas
from reportlab.platypus import Paragraph, Table, TableStyle
from reportlab.lib.styles import ParagraphStyle
from reportlab.lib.enums import TA_JUSTIFY, TA_CENTER
from reportlab.graphics.renderPM import PMCanvas

import random
import time

import threading
from threading import Event


class Ui_Main(QMainWindow):
    def __init__(self):        
        super(Ui_Main, self).__init__()
        uic.loadUi("IGmain.ui", self)
        self.setWindowIcon(QIcon('images\\0.ico'))

        self.win = pg.GraphicsLayoutWidget(show=True)
        self.cursorlabel = pg.LabelItem(justify='right')
        self.win.addItem(self.cursorlabel)
        self.plt_wid = self.win.addPlot(row=1, col=0)
        self.plt_wid.showGrid(True, True)
        self.plt_wid.setLabel('bottom', 'Energy (keV)')
        self.plt_wid.setLabel('left', 'Intensity', '# Counts')
        self.plotx = []
        self.ploty = []
        self.plotx_2 = []
        self.ploty_2 = []
        self.plt_wid.addLegend()
        self.plt_data = self.plt_wid.plot(self.plotx, self.ploty, name="Experimental",
                                          symbolSize=0.5, pen=(255,0,0),
                                          symbolBrush=(255,0,0), symbolPen='r')

        self.plt_data2 = self.plt_wid.plot(self.plotx_2, self.ploty_2, name="Modelling",
                                           symbolSize=0.5, pen=(0,0,255),
                                           symbolBrush=(0,0,255), symbolPen='b')
        
        self.gridLayout.addWidget(self.win)
        self.plt_wid.scene().sigMouseMoved.connect(self.mouseMoved)

        #---------------------------- Lateral Menu ----------------------------#

        self.ElementsBtn.clicked.connect(self.AgrandarMenuLateral)
        
        #---------------------------- File's Actions ----------------------------#
        self.New = self.File.addAction("New")
        self.File.addSeparator()
        #self.New.triggered.connect(self.Nuev)        
        self.Abrir = self.File.addAction("Open (Modeling)", self.InsertarEspectro)       

        self.Insertar = self.File.addAction("Insert (Experimental)", self.Insert)        
        self.File.addSeparator()

        self.Guardar = self.File.addAction("Save", self.Guard)
        self.Guardar.setEnabled(False)      

        self.GuardarComo = self.File.addAction("Save As...", self.GuardarCom)
        self.File.addSeparator()           
             
        self.Salir = self.File.addAction("Close", self.close)

        #---------------------------- Configuration's Actions ----------------------------#
        self.Calibracion = self.Configuration.addMenu("Calibration")
        self.Energetica = self.Calibracion.addAction("Energetic", self.Calibracio)        
        self.Canales = self.Calibracion.addAction("Full width at half maximum", self.AnchoMediaAltura)        
        
        
        self.Modelamiento = self.Configuration.addMenu("Modelling")
        self.Configuration.addSeparator()        
        self.Funcion = self.Modelamiento.addAction("Func. Distr. Energ.", self.Distribucio)        
        
        self.Tecnica = self.Configuration.addMenu("Type of Technique")
        self.Convencional = self.Tecnica.addAction("Conventional", self.Convenciona)
        self.Convencional.setCheckable(True)
        self.Convencional.setChecked(True)
        self.Polarizacion = self.Tecnica.addAction("Polarization")
                
        self.Matriz = self.Configuration.addMenu("Matrix")
        self.Oxido = self.Matriz.addAction("Oxide", self.Oxid)
        self.Oxido.setCheckable(True)
        self.Oxido.setChecked(True)
        self.lineEdit_8.setEnabled(False)
        
        self.General = self.Matriz.addAction("General", self.Genera)
        self.General.setCheckable(True)
        self.General.setChecked(False)        

        #---------------------------- Graph's Actions ----------------------------#

        self.Logaritmico = self.Graph.addAction("Logarithmic", self.Logaritmo)
        self.Logaritmico.setCheckable(True)
        self.Logaritmico.setChecked(True)
        
        self.Lineal = self.Graph.addAction("Linear", self.Linea)
        self.Lineal.setCheckable(True)
        self.Lineal.setChecked(False)

        
        #---------------------------- About's Actions ----------------------------#
        self.Software = self.About.addAction("Software", self.AboutTheSoft)   

        #---------------------------- Additional Buttons ----------------------------#
         

        self.ModellingBtn.clicked.connect(self.Calculos)     
        self.MakeBtn.clicked.connect(self.Informe)
        self.ExportBtn.clicked.connect(self.Exporta)     

        self.run()
    
    def Exporta(self):
        try:
            
            wb2 = openpyxl.Workbook()
            sheet = wb2.active
            
            thin = Side(border_style="thin", color="FFFFFF") #color="CCFFFF"
            border = Border(left=thin, right=thin, top=thin, bottom=thin)

            ct = sheet.cell(row = 1, column = 1)
            ct.value = 'Energy'
            ct.border = border

            ct = sheet.cell(row = 1, column = 2)
            ct.value = 'Experimental'
            ct.border = border

            ct = sheet.cell(row = 1, column = 3)
            ct.value = 'Modeling'
            ct.border = border

            for i in range(self.Posici1kev[0], len(self.E)):
                ct = sheet.cell(row = 2+i-self.Posici1kev[0], column = 1)
                ct.value = self.E[i]

                ct = sheet.cell(row = 2+i-self.Posici1kev[0], column = 2)
                ct.value = self.ploty[i]

                ct = sheet.cell(row = 2+i-self.Posici1kev[0], column = 3)
                ct.value = self.Espe[i]



            name = QFileDialog.getSaveFileName(None, caption='Select a data file',
                                                   filter='Archivos de Excel (*.xlsx)')
    
            wb2.save(name[0])

            mesa='Excel file generated successfully'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                color: white;''')
            msg.setIcon(QMessageBox.Information)
            msg.setText("Well done!")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
            


        except Exception as e:
            print(e)
            mesa='Could not generate excel file'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                color: white;''')
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error!")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()

    def Informe(self):
        try:
            self.nombreAr = QFileDialog.getSaveFileName(None, caption='Select a data file',
                                           filter='PDF (*.pdf)')
            
            registerFont (TTFont ('Arial', 'ARIAL.ttf'))
            registerFont (TTFont ('Book', 'BOOKOSB.ttf'))
            registerFont (TTFont ('Arial-Bold', 'ARIALBd.ttf'))
            canv = PMCanvas (10,10)

            fileName = self.nombreAr[0]

            c = canvas.Canvas(fileName, pagesize=A4)
            p = ParagraphStyle(fileName)
            p.textColor = 'black'
            p.alignment = TA_JUSTIFY

            c.setFont ('Book', 13) #Times-Bold
            title="UNIVERSIDAD NACIONAL MAYOR DE SAN MARCOS"
            c.drawString(120,770,title)
            tamle = int(10)
            c.setFont ('Arial', 12)
            subtitle = "Laboratorio de Arqueometría y Análisis de Suelos"
            c.drawString(163,750,subtitle)

            c.setFont ('Arial', tamle)
            c.drawString(30,710,"Resultados del modelamiento físico del espectro experimental obtenido mediante fluorescencia de rayos-X dispersiva")
            c.drawString(30,700, "en energía.")

            c.setFont ('Arial-Bold', tamle)
            c.drawString(30,680,"Tipo de técnica: ")

            c.setFont ('Arial', tamle)

            if (self.Convencional.isChecked() == True):
                tipo_tec = "Convencional (Función de distribución energética)"
            elif (self.Polarizacion.isChecked() == True):
                tipo_tec = "Blanco secundario (Polarización de la radiación)"

            c.drawString(120,680,tipo_tec)

            c.line(30,670,550,670)

            c.drawImage('grafica.png', 5, 380, 590, 280)

            
            ElEnc = self.ElEnc
            ElConc = self.ElConc
            aste = self.aste
            asterisco = self.asterisco
            Incer = self.Incer
            RxSimu = self.RxSimu
        
            c.setFont ('Arial', 9)

            c.drawString(85,360,"Elementos")
            c.drawString(250,360,"Concentración")
            c.drawString(270,350,"(%)")
            c.drawString(425,360,"Rayos-X simulados")

            c.line(80,345,510,345)
            
            for i in range(0, len(ElEnc)):
                c.drawString(87,330-12*i, ElEnc[i]+aste[i])
                c.drawString(250,330-12*i,str(round(float(ElConc[i]),5))+" ± "+str(round(2*float(Incer[i]),5)))
                c.drawString(425,330-12*i,str(int(RxSimu[i])))

            c.line(80,330-12*len(ElEnc),510,330-12*len(ElEnc))
            
            if (asterisco == "si"):
                c.drawString(90,320-12*len(ElEnc)," *:   µg/g ")

            NameTitle = str(fileName).split('/')
            NameTitle = NameTitle[len(NameTitle)-1]
            NameTitle = str(NameTitle).split('.')
            NameTitle = NameTitle[0]

            
            c.setFont ('Arial-Bold', tamle)
            c.drawString(30,655,"Nombre muestra: ")

            c.setFont ('Arial', tamle)            
            c.drawString(120,655,NameTitle)
            
            c.save()

            

            mesa='Report generated successfully'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                color: white;''')
            msg.setIcon(QMessageBox.Information)
            msg.setText("Well done!")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
            


        except Exception as e:
            print(e)
            mesa='Could not generate report'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                color: white;''')
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error!")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
    
    def InsertarEspectro(self):

        try:
            filename = QFileDialog.getOpenFileName(None, 'Abrir Archivo', filter='Archivos Model X-Ray (*.mrx)',
                                                             options=QFileDialog.Options())
            self.Name = filename            

            if filename[0]:                
                with open(filename[0],'r') as Info:
                    Info.readline()
                    Lin1 = str(Info.readline())
                    Lin2 = str(Info.readline())
                    Lin3 = str(Info.readline())
                    Lin4 = str(Info.readline())

                    if (Lin4 == " \n"):
                        if (Lin3 == " \n"):
                            Datos = open("Calibracion.txt",'w')
                            Datos.write(Lin1)
                            Datos.write(Lin2)
                            Datos.close()
                        else:
                            Datos = open("Calibracion.txt",'w')
                            Datos.write(Lin1)
                            Datos.write(Lin2)
                            Datos.write(Lin3)
                            Datos.close()
                    else:
                        Datos = open("Calibracion.txt",'w')
                        Datos.write(Lin1)
                        Datos.write(Lin2)
                        Datos.write(Lin3)
                        Datos.write(Lin4)
                        Datos.close()
                    
                    Real_Time = str(Info.readline()).split('\n')
                    self.Real_Time = float(Real_Time[0])
                    
                    Live_Time = str(Info.readline()).split('\n')
                    self.Live_Time = float(Live_Time[0])

                    Lin1 = str(Info.readline())
                    Lin2 = str(Info.readline())
                    Lin3 = str(Info.readline())
                    Lin4 = str(Info.readline())
                    Lin5 = str(Info.readline())

                    if (Lin5 != " \n" and Lin4 != " \n" and Lin3 != " \n" and Lin2 != " \n" and Lin1 != " \n"):
                        Datos = open("Ancho.txt",'w')
                        Datos.write(Lin1)
                        Datos.write(Lin2)
                        Datos.write(Lin3)
                        Datos.write(Lin4)
                        Datos.write(Lin5)
                        Datos.close()

                    elif (Lin5 == " \n" and Lin4 != " \n" and Lin3 != " \n" and Lin2 != " \n" and Lin1 != " \n"):
                        Datos = open("Ancho.txt",'w')
                        Datos.write(Lin1)
                        Datos.write(Lin2)
                        Datos.write(Lin3)
                        Datos.write(Lin4)
                        Datos.close()

                    elif (Lin5 == " \n" and Lin4 == " \n" and Lin3 != " \n" and Lin2 != " \n" and Lin1 != " \n"):
                        Datos = open("Ancho.txt",'w')
                        Datos.write(Lin1)
                        Datos.write(Lin2)
                        Datos.write(Lin3)                        
                        Datos.close()

                    elif (Lin5 == " \n" and Lin4 == " \n" and Lin3 == " \n" and Lin2 != " \n" and Lin1 != " \n"):
                        Datos = open("Ancho.txt",'w')
                        Datos.write(Lin1)
                        Datos.write(Lin2)                                            
                        Datos.close()

                    Blanco = str(Info.readline()).split('\n')

                    Datos = open('BlancoSeleccionado.txt','w')
                    Datos.write(Blanco[0])
                    Datos.close()

                    Lin1 = str(Info.readline()).split('\n')
                    Lin2 = str(Info.readline()).split('\n')
                    Lin3 = str(Info.readline()).split('\n')
                    Lin4 = str(Info.readline()).split('\n')
                    Lin5 = str(Info.readline()).split('\n')
                    Lin6 = str(Info.readline()).split('\n')
                    Lin7 = str(Info.readline()).split('\n')
                    Lin8 = str(Info.readline()).split('\n')
                    Lin9 = str(Info.readline()).split('\n')
                    
                    Datos = open('FuncionBlancoSecundario.txt','w')
                    Datos.write(Lin1[0]+";"+Lin2[0]+";"+Lin3[0]+";"+Lin4[0]+";"+Lin5[0]+";"+Lin6[0]+";"+Lin7[0]+";"+Lin8[0]+";"+Lin9[0])
                    Datos.close()

                    Lin1 = str(Info.readline()).split('\n')
                    Lin2 = str(Info.readline()).split('\n')
                    Lin3 = str(Info.readline()).split('\n')
                    Lin4 = str(Info.readline()).split('\n')
                    Lin5 = str(Info.readline()).split('\n')
                    Lin6 = str(Info.readline()).split('\n')
                    Lin7 = str(Info.readline()).split('\n')
                    Lin8 = str(Info.readline()).split('\n')
                    Lin9 = str(Info.readline()).split('\n')
                    Lin10 = str(Info.readline()).split('\n')
                    Lin11 = str(Info.readline()).split('\n')
                    Lin12 = str(Info.readline()).split('\n')
                    Lin13 = str(Info.readline()).split('\n')

                    Datos = open('FuncionDistribucion.txt','w')
                    Datos.write(Lin1[0]+";"+Lin2[0]+";"+Lin3[0]+";"+Lin4[0]+";"+Lin5[0]+";"+Lin6[0]+";"+Lin7[0]+";"+Lin8[0]+";"+Lin9[0]+";"+Lin10[0]+";"+Lin11[0]+";"+Lin12[0]+";"+Lin13[0])
                    Datos.close()
                
                    Tecnica = str(Info.readline()).split('\n')
                    Tecnica = Tecnica[0]

                    if (Tecnica == "Convencional"):
                        self.Convencional.setChecked(True)
                        self.Polarizacion.setChecked(False)

                    elif (Tecnica == "Polarizacion"):
                        self.Convencional.setChecked(False)
                        self.Polarizacion.setChecked(True)
                                                    
                    Matriz = str(Info.readline()).split('\n')
                    Matriz = Matriz[0]

                    if (Matriz == "Oxido"):
                        self.Oxido.setChecked(True)
                        self.General.setChecked(False)
                        self.lineEdit_8.setEnabled(False)

                    elif (Matriz == "General"):
                        self.Oxido.setChecked(False)
                        self.General.setChecked(True)
                        self.lineEdit_8.setEnabled(True)


                    cantiCana=2049
                    
                    x=np.array(list(range(1,cantiCana))) # Eje X
                    
                    self.Calibracion=np.loadtxt("Calibracion.txt")
                    
                    self.E = Minimos.Cuadrados(self.Calibracion[:,0],self.Calibracion[:,1],1,x)
                    self.plotx = self.E

                    self.Vec_Dat=np.zeros(2048)
                    for i in range(0,2048):
                        Lin = str(Info.readline()).split('\n')
                        self.Vec_Dat[i]=float(Lin[0])
                    
                    self.ploty = self.Vec_Dat

                    self.Vec_Datos = self.Vec_Dat

                    if self.Lineal.isChecked():
                        self.plt_data.setLogMode(False, False)
                    else:
                        self.plt_data.setLogMode(False, True)

                    Posi=np.array(np.where(self.E>1)) ## Para interpolación efectiva (debido a que se dispone de datos de los coef. posteriores a 1 keV)
                    self.Posici1kev=np.array(Posi[0]) ## Posición posterior a 1 keV
                        
                    self.plt_data.setData(self.plotx[self.Posici1kev[0]:len(self.E)], self.ploty[self.Posici1kev[0]:len(self.E)], name='experimental')
          
                    for i in range(1,99):
                        Lin = str(Info.readline()).split('\n')
                        if not (float(Lin[0]) == 0):
                            getattr(self, "lineEdit_{}".format(i)).setText(Lin[0])
                        else:
                            getattr(self, "lineEdit_{}".format(i)).setText("")

                    self.Guardar.setEnabled(True)  

            self.Calculos()
                            
                    

        except Exception as e:
            print(e)
            mesa='Archivo incorrecto o dañado'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Error! ")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Simu X-Ray")
            msg.exec_()

    def Calculos(self):
        try:
            self.Calibracion = np.loadtxt("Calibracion.txt")
            self.Ancho = np.loadtxt("Ancho.txt")

            Efic = np.loadtxt('DetecEfi.txt')
            self.Energ = Efic[:,0]
            self.Efficiency = Efic[:,1]

            DensidadAire = 0.00118906987 #------------------ g/cm3

            FTEscape = 0 #---------------------------------- Space Peaks
            FTSuma = 0   #---------------------------------- Sum Peaks

            #----------------------------------------------------------- Estimation of dead time -----------------------------------------------------------#
            Cont_Tot = sum(self.ploty)
            C1 = Cont_Tot / self.Real_Time
            C0 = Cont_Tot / self.Live_Time
            Tao = (C0 - C1) / (C0 * C1) #------------------ average dead time

            if(self.Convencional.isChecked()):

                d = 2 #------------------------------------ cm this is for conventional technique                 
                Ratio = 0.15 #--------------------------------- cm detector area
                AngSolBl = np.pi * Ratio ** 2 / d ** 2

                Fun = open('FuncionDistribucion.txt', 'r') #--------------------- This part can ben changed 
                FunD = str(Fun.readline()).split(';') #-------------------------- because it depends exclusively 
                Fun.close() #---------------------------------------------------- on the X-ray tube.
                FIO = float(FunD[0]) * 10 ** 6 #--------------------------------- You can propose your model here.
                p = float(FunD[1]) #---------------------------------------------
                E01 = float(FunD[2]) 
                E21 = float(FunD[3])
                E31 = float(FunD[4])
                r = float(FunD[5])
                alfa1 = float(FunD[6])
                alfa2 = float(FunD[7])
                var = float(FunD[8])
                IL = float(FunD[9])
                Cur = float(FunD[10])
                Cu2 = float(FunD[11])
                Ecol = float(FunD[12])

                cantiCana = 2049 #--------------------------------------------- Defines the number of channels
                x = np.array(list(range(1, cantiCana))) 
                self.E = Minimos.Cuadrados(self.Calibracion[:, 0], self.Calibracion[:, 1], 1, x)

                #--------------------------------------- It is considered a function of continuous energy distribution (Target Au)  ---------------------------------------#
                g = np.exp(-(self.E - E01) / (2 * (var) ** 2)) / np.sqrt(2 * mt.pi * (var) ** 2)                
                f = ((alfa1 ** (p + 1)) * (self.E ** p) * np.exp(-alfa1 * self.E) * (1 + r * g)) / ((1 + np.exp(E31 - self.E)) * (1 + np.exp(alfa2 * (self.E - E21))))                
                N = 1 / sum(f) #----------------------------------------------- Normalization factor estimation
                Fn = f * N * FIO

                #self.plt_data2.setData(self.E, Fn) #----------- This line is only to see distribution function

                lista = ['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al', 'Si', 'P', 'S','Cl','Ar','K','Ca',
                        'Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y','Zr',
                        'Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd',
                        'Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg',
                        'Tl','Pb','Bi','Po','At','Rn','Fr','Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf']

                if self.General.isChecked():
                    Conc = np.zeros(98)
                    for i in range(1,99):
                        try:
                            Conc[i-1] = float(getattr(self, "lineEdit_{}".format(i)).text())
                        except:
                            Conc[i-1] = 0
                    
                    uT = np.dot(Conc, self.umaT) #----------------------------- Matrix multiplication to obtain the Total coefficient

                    #---------------------------------------- Calculating the total density ----------------------------------------#
                    rho = np.array([0.09, 0.18, 0.53, 1.85, 2.34, 2.26, 1.25, 1.43, 1.7, 0.9, 0.97, 1.74, 2.72, 2.33, 1.82, 2, 1.56, 0.001784, 0.862, 1.55, 
                                    2.99, 4.54, 6.11, 7.19, 7.42, 7.86, 8.90, 8.90, 8.94, 7.14, 5.90, 5.32, 5.73, 4.79, 3.12, 0.00374, 1.53, 2.54, 4.40, 6.53, 
                                    8.57, 10.22, 11.5, 12.41, 12.44, 12.16, 10.5, 8.65, 7.28, 5.31, 6.69, 6.24, 4.94, 0.0059, 1.873, 3.5, 6.15, 6.67, 6.77, 6.96, 
                                    6.782, 7.536, 5.259, 7.95, 8.27, 8.536, 8.80, 9.05, 9.33, 6.977, 9.84, 13.3, 16.6, 19.299, 21.02, 22.5, 22.42, 21.37, 19.37, 
                                    13.55, 11.86, 11.34, 9.8, 9.3, 6.4, 0.00973, 1.87, 5.5, 10.07, 11.70, 15.4, 19.05, 20.2, 19.84, 13.67, 13.5, 14.78, 15.1])

                    Concrho = np.zeros(len(rho))
                    for i in range(0, len(rho)):
                        Concrho[i] = rho[i] * Conc[i]

                    uD = np.dot(Concrho, self.umaD) #--------------------------- Matrix multiplication to obtain the Total scattering
                    uI = np.dot(Concrho, self.umaI) #--------------------------- Matrix multiplication to obtain the Compton scattering
                    
                    Dens = 0
                    for i in range(0, len(rho)):
                        Dens = Dens + Conc[i] / rho[i]
                        
                    DensidadBlanco = 1 / Dens
                
                elif self.Oxido.isChecked():
                    
                    Conc=np.zeros(98)
                    for i in range(1, 99):
                        try:
                            Conc[i-1] = float(getattr(self, "lineEdit_{}".format(i)).text())
                        except:
                            Conc[i-1] = 0

                    COxidoNa = Conc[10] * 100 / 74.1857 #   74.1857 % is the concentration of Na in Sodium Oxide (Na2O)
                    self.COxigeno_1 = 0.258143 * COxidoNa
                    
                    COxidoAl = Conc[12] * 100 / 52.9251 #   52.9251 % is the concentration of Al in Alumina (Al2O3)
                    self.COxigeno_2 = 0.470749 * COxidoAl

                    COxidoSi = Conc[13] * 100 / 46.7435 #   46.7435 % is the concentration of Si in Silice (SiO2)
                    self.COxigeno_3 = 0.532565 * COxidoSi

                    COxidoK = Conc[18] * 100 / 83.0148 #    83.0148 % is the concentration of K in Potassium Oxide (K2O)
                    self.COxigeno_4 = 0.169852 * COxidoK

                    COxidoCa = Conc[19] * 100 / 71.4691 #   71.4691 % is the concentration of Ca in Calcium Oxide (CaO)
                    self.COxigeno_5 = 0.285309 * COxidoCa

                    COxidoMn = Conc[24] * 100 / 63.1931 #   63.1931 % is the concentration of Mn in Manganese Oxide (MnO2)
                    self.COxigeno_6 = 0.368069 * COxidoMn

                    COxidoFe = Conc[25] * 100 / 69.9433 #   69.9433 % is the concentration of Fe in Ferric Oxide (Fe2O3)
                    self.COxigeno_7 = 0.300567 * COxidoFe

                    COxidoZn = Conc[29] * 100 / 80.3422 #   80.3422 % is the concentration of Zn in Zinc Oxide (ZnO)
                    self.COxigeno_8 = 0.196578 * COxidoZn

                    COxidoSr = Conc[37] * 100 / 84.5595 #   84.5595 % is the concentration of Sr in Strontium Oxide (SrO)
                    self.COxigeno_9 = 0.154405 * COxidoSr

                    COxidoSb = Conc[50] * 100 / 83.5340 #   83.5340 % is the concentration of Sb in Antimony Oxide (Sb2O3)
                    self.COxigeno_10 = 0.164660 * COxidoSb

                    COxidoBa = Conc[55] * 100 / 89.5651 #   89.5651 % is the concentration of Ba in Barium Oxide (BaO)
                    self.COxigeno_11 = 0.104349 * COxidoBa

                    COxidoLa = Conc[56] * 100 / 85.2680 #   85.2680 % is the concentration of La in Lanthanum Oxide (La2O3)
                    self.COxigeno_12 = 0.147320 * COxidoLa

                    COxidoHg = Conc[79] * 100 / 92.6130 #   92.6130 % is the concentration of Hg in Mercury Oxide (HgO)
                    self.COxigeno_13 = 0.073870 * COxidoHg

                    COxidoPb = Conc[81] * 100 / 86.6225 #   86.6225 % is the concentration of Pb in Lead Oxide (PbO2)
                    self.COxigeno_14 = 0.133775 * COxidoPb

                    COxidoU = Conc[91] * 100 / 88.1498  #   88.1498 % is the concentration of Pb in Uranium Oxide (UO2)
                    self.COxigeno_15 = 0.118502 * COxidoU

                    CO = 0
                    for i in range(1, 16):
                        CO = CO + float(getattr(self, "COxigeno_{}".format(i)))

                    self.lineEdit_8.setText(str(round(CO, 7)))

                    for i in range(1, 99):
                        try:
                            Conc[i-1] = float(getattr(self, "lineEdit_{}".format(i)).text())
                        except:
                            Conc[i-1] = 0

                    uT = np.dot(Conc, self.umaT) #----------------------------- Matrix multiplication to obtain the Total coefficient

                    #---------------------------------------- Calculating the total density ----------------------------------------#
                    rho = np.array([0.09, 0.18, 0.53, 1.85, 2.34, 2.26, 1.25, 1.43, 1.7, 0.9, 0.97, 1.74, 2.72, 2.33, 1.82, 2, 1.56, 0.001784, 0.862, 1.55, 
                                    2.99, 4.54, 6.11, 7.19, 7.42, 7.86, 8.90, 8.90, 8.94, 7.14, 5.90, 5.32, 5.73, 4.79, 3.12, 0.00374, 1.53, 2.54, 4.40, 6.53,
                                     8.57, 10.22, 11.5, 12.41, 12.44, 12.16, 10.5, 8.65, 7.28, 5.31, 6.69, 6.24, 4.94, 0.0059, 1.873, 3.5, 6.15, 6.67, 6.77, 6.96, 
                                     6.782, 7.536, 5.259, 7.95, 8.27, 8.536, 8.80, 9.05, 9.33, 6.977, 9.84, 13.3, 16.6, 19.299, 21.02, 22.5, 22.42, 21.37, 19.37, 
                                     13.55, 11.86, 11.34, 9.8, 9.3, 6.4, 0.00973, 1.87, 5.5, 10.07, 11.70, 15.4, 19.05, 20.2, 19.84, 13.67, 13.5, 14.78, 15.1])
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                    Concrho = np.zeros(len(rho))
                    for i in range(0, len(rho)):
                        Concrho[i] = rho[i] * Conc[i]
                    
                    uD = np.dot(Concrho, self.umaD) #--------------------------- Matrix multiplication to obtain the Total scattering
                    uI = np.dot(Concrho, self.umaI) #--------------------------- Matrix multiplication to obtain the Compton scattering

                    ConMol = np.zeros(97)

                    for i in range(1, 8):
                        try:
                            ConMol[i-1] = float(getattr(self, "lineEdit_{}".format(i)).text())
                        except:
                            ConMol[i-1] = 0

                    for i in range(9, 99):
                        try:
                            ConMol[i-2] = float(getattr(self, "lineEdit_{}".format(i)).text())
                        except:
                            ConMol[i-2] = 0

                    ConMol[9] = COxidoNa
                    ConMol[11] = COxidoAl
                    ConMol[12] = COxidoSi
                    ConMol[17] = COxidoK
                    ConMol[18] = COxidoCa
                    ConMol[23] = COxidoMn
                    ConMol[24] = COxidoFe
                    ConMol[28] = COxidoZn
                    ConMol[36] = COxidoSr
                    ConMol[49] = COxidoSb
                    ConMol[54] = COxidoBa
                    ConMol[55] = COxidoLa
                    ConMol[78] = COxidoHg
                    ConMol[80] = COxidoPb
                    ConMol[90] = COxidoU
                    
                    rhoMol = np.array([0.09, 0.18, 0.53, 1.85, 2.34, 2.26, 1.25, 1.7, 0.9, 2.27, 1.74, 3.95, 2.634, 1.82, 2, 1.56, 0.001784, 2.35, 3.3, 
                                       2.99, 4.54, 6.11, 7.19, 5.026, 5.24, 8.90, 8.90, 8.94, 5.61, 5.90, 5.32, 5.73, 4.79, 3.12, 0.00374, 1.53, 4.7, 4.40,
                                        6.53, 8.57, 10.22, 11.5, 12.41, 12.44, 12.16, 10.5, 8.65, 7.28, 5.31, 5.2, 6.24, 4.94, 0.0059, 1.873, 5.72, 6.51, 6.67, 
                                        6.77, 6.96, 6.782, 7.536, 5.259, 7.95, 8.27, 8.536, 8.80, 9.05, 9.33, 6.977, 9.84, 13.3, 16.6, 19.299, 21.02, 22.5, 22.42, 
                                        21.37, 19.37, 11.14, 11.86, 9.38, 9.8, 9.3, 6.4, 0.00973, 1.87, 5.5, 10.07, 11.70, 15.4, 10.97, 20.2, 19.84, 13.67, 13.5, 
                                        14.78, 15.1])
                    
                    Dens = 0
                    for i in range(0, len(rhoMol)):
                        Dens = Dens + ConMol[i] / rhoMol[i]
                        
                    DensidadBlanco = 1 / Dens


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------------------- Scattering ---------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                Fc = np.zeros(len(self.E))
                    
                for i in range(self.Posici1kev[0], len(self.E)):
                    Fc[i] = Fc[i] + (3 * AngSolBl / (4 * np.pi)) * Fn[i] * uD[i - self.Posici1kev[0]] / (uT[i - self.Posici1kev[0]] * DensidadBlanco) # El 4 inicialmente era 16
                
                #------------------------------------------------ Added Compton Scaterring ------------------------------------------------#                
                
                FcI = np.zeros(len(self.E))

                for i in range(self.Posici1kev[0], len(self.E)):
                    try:
                        EDine = 1 / (1 / 511 + 1 / self.E[i])

                        if (EDine > 1):

                            PosiI = np.array(np.where(self.E > EDine))
                            PosicI = np.array(PosiI[0])
                            PosiI = int(np.array(PosicI[0]))

                            FcI[i] = FcI[i] + (3 * AngSolBl / (8 * np.pi)) * Fn[i] * uI[i - self.Posici1kev[0]] / ((uT[i - self.Posici1kev[0]] + uT[int(PosiI)-self.Posici1kev[0]])* DensidadBlanco) # El 4 inicialmente era 16
                    except:
                        pass
                

                #--------------------------------------------- Insert geometry fact (experimental) ---------------------------------------#

                Fact_G = np.loadtxt("FacGeo.txt")
                
                #--------------------------------------------------------- Ecol ---------------------------------------------------------#
                Posi22 = np.array(np.where(self.E > Ecol))
                Posic = np.array(Posi22[0])
                Posi2 = int(np.array(Posic[0]))

                AddFc = np.zeros(len(self.E))

                for i in range(self.Posici1kev[0], Posi2):
                    AddFc[i] = (81.422 * self.E[i] ** 3 - 190.73 * self.E[i] ** 2 + 72.595 * self.E[i] + 187.49) * Cu2 + Cur

                Fc = Fc + AddFc + FcI

                self.Fc = Fc

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------- Gold's X-ray Scattering ------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                ProAu = loadtxt('data\ProAu.txt') # Probability that a specific type of X-ray will be generated                     
                ProAuE = ProAu[:,0]
                ProAuRx = ProAu[:,1]

                ProAuQx = loadtxt('data\DatAu.txt') # Energy jump probability and X-ray efficiency

                SAu = ProAuQx[0,:]
                WAu = ProAuQx[1,:]

                IL = IL * 10 ** 6
                
                IL1 = np.zeros(9)
                IL2 = np.zeros(7)
                IL3 = np.zeros(8)
                IM1 = np.zeros(2)

                #'IL1
                for i in range(0, len(IL1)):
                    IL1[i] = IL * ((SAu[1] - 1) / SAu[1]) * WAu[1] * ProAuRx[i + 5]

                #'IL2
                for i in range(0, len(IL2)):
                    IL2[i] = IL * ((SAu[2] - 1) / SAu[2]) * WAu[2] * ProAuRx[i + 14]

                #'IL3
                for i in range(0, len(IL3)):
                    IL3[i] = IL * ((SAu[3] - 1) / SAu[3]) * WAu[3] * ProAuRx[i + 21]

                #'IM1
                for i in range(0, len(IM1)):
                    IM1[i] = IL * ((SAu[4] - 1) / (4 * SAu[4] * SAu[3] * SAu[2] * SAu[1])) * WAu[4] * ProAuRx[i + 29]

                ILT = np.concatenate((IL1, IL2, IL3, IM1), axis = 0)


                TAuDRx = np.zeros(len(ProAuE[5 : 31]))
                PosiAu = np.zeros(len(ProAuE[5 : 31]))
                
                for i in range(0, len(TAuDRx)):
                    Posi22 = np.array(np.where(self.E > ProAuE[i + 5]))
                    Posic = np.array(Posi22[0])
                    Posi2 = int(np.array(Posic[0]))
                    PosiAu[i] = Posi2

                    TAuDRx[i] = ILT[i] / ((3 * AngSolBl / (4 * np.pi)) * uD[Posi2 - self.Posici1kev[0]] / (uT[Posi2 - self.Posici1kev[0]] * DensidadBlanco))

                
                AnchAu = Minimos.Cuadrados(self.Ancho[:, 0], self.Ancho[:, 1], 1, ProAuE[5 : 31])
                SigAu = AnchAu / (2 * np.sqrt(2 * np.log(2)))                    

                FAu = np.zeros([len(ProAuE[5 : 31]), len(self.E)])
                FTAuD = np.zeros(len(self.E))

                for i in range(0, len(ProAuE[5 : 31])):
                    FAu[i,:] = round(ILT[i] * Interpolacion.Lineal(self.Energ, self.Efficiency, ProAuE[i + 5])) * Distribucion.GaussLorentz(self.E, SigAu[i], ProAuE[i + 5])

                    FTAuD = FTAuD + FAu[i, :]

                #------------------------------------------------------------------------------------------------------#
                
                EAui = ProAuE[5 : 31]
                AuED = np.zeros(len(EAui))
                ILTin = np.zeros(len(EAui))

                for i in range(0, len(AuED)):
                    AuED[i] = 1 / (1 / 511 + 1 / EAui[i]) #Energy producted by compton scattering to 90°
                
                
                PosiAuD = np.zeros(len(AuED))
                for i in range(0, len(AuED)):
                    Posi22 = np.array(np.where(self.E > AuED[i]))
                    Posic = np.array(Posi22[0])
                    Posi2 = int(np.array(Posic[0]))
                    PosiAuD[i] = Posi2

                    ILTin[i] = TAuDRx[i] * (3 * AngSolBl / (4 * np.pi)) * uI[int(PosiAu[i]) - self.Posici1kev[0]] / ((uT[int(PosiAu[i]) - self.Posici1kev[0]] + uT[int(Posi2)-self.Posici1kev[0]]) * DensidadBlanco)
                
                AnchAui = Minimos.Cuadrados(self.Ancho[:,0], self.Ancho[:,1], 1, AuED)
                SigAui = AnchAui / (2 * np.sqrt(2 * np.log(2)))

                FAui = np.zeros([len(AuED), len(self.E)])
                FTAuDi = np.zeros(len(self.E))

                for i in range(0, len(AuED)):
                    FAui[i,:] = round(ILTin[i] * Interpolacion.Lineal(self.Energ, self.Efficiency, AuED[i])) * Distribucion.GaussLorentz(self.E, SigAui[i], AuED[i])

                    FTAuDi = FTAuDi + FAui[i, :]
            
                #' Gold's x-ray incident on the sample can ionize elements such as: Zn, Cu, Ni, Co, Fe, Mn, ... L Ag.

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------          For Radium          ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CRn=Conc[85]
                rhoRn=0.00973 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-------------------------------------------          For Bismuth          ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CBi=Conc[82]
                rhoBi=9.8 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------          For Lead          -----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CPb=Conc[81]
                rhoPb=11.34 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-------------------------------------------          For Thallium         ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CTl=Conc[80]
                rhoTl=11.86 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------          For Mercury          ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CHg=Conc[79]
                rhoHg=13.55 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #---------------------------------------------          For Gold          -----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CAu=Conc[78]
                rhoAu=19.37 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-------------------------------------------          For Platinum          ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CPt=Conc[77]
                rhoPt=21.37 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------          For Iridium         ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CIr=Conc[76]
                rhoIr=22.42 #g/cm3

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------          For Osmium          ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                COs=Conc[75]
                rhoOs=22.5 #g/cm3    

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #---------------------------------------------          For Silver         ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                        
                CAg = Conc[46]
                rhoAg = 10.49 #g/cm3
                
                ProAg = loadtxt('data\ProAg.txt') # Probability that a specific type of X-ray will be generated                      
                ProAgE = ProAg[:,0]
                ProAgRx = ProAg[:,1]

                ProAgQx = loadtxt('data\DatAg.txt') # Energy jump probability and X-ray efficiency

                SAg = ProAgQx[0,:]
                WAg = ProAgQx[1,:]
                
                Posi2 = np.zeros(len(ProAgE))
                for i in range(0, len(ProAgE)):
                    Posi22 = np.array(np.where(self.E > ProAgE[i]))
                    Posic = np.array(Posi22[0])
                    Posi2[i] = np.array(Posic[0])

                TkaAgRx = np.zeros(len(Posi2))
                
                ## For Layer K ##
                Posi_1 = np.array(np.where(self.E > 25.514)) ## Ionization Potential of Ag for the K layer
                Posici_1 = np.array(Posi_1[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the K layer in Ag
                
                for j in range(0, 5):
                    for i in range(Posici_1[0], len(self.E)):
                        TkaAgRx[j] = TkaAgRx[j] + CAg * Fn[i] * (WAg[0] * (SAg[0] - 1) / SAg[0]) * ProAgRx[j] * (rhoAg / DensidadBlanco) * self.umaEF[46,i - self.Posici1kev[0]] / (uT[i - self.Posici1kev[0]] + uT[int(Posi2[j]) - self.Posici1kev[0]])

                
                ## For Layer L1 ##
                Posi_2 = np.array(np.where(self.E > 3.806)) ## Ionization Potential of Ag for the L1 layer                
                Posici_2 = np.array(Posi_2[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L1 layer in Ag

                for j in range(5, 9):                        
                    for i in range(Posici_2[0], Posici_1[0]):
                        TkaAgRx[j] = TkaAgRx[j] + CAg * Fn[i] * (WAg[1] * (SAg[1] - 1) / SAg[1]) * ProAgRx[j] * (rhoAg / DensidadBlanco) * self.umaEF[46,i - self.Posici1kev[0]] / (uT[i - self.Posici1kev[0]] + uT[int(Posi2[j]) - self.Posici1kev[0]])
                    
                    for i in range(Posici_1[0], len(self.E)):
                        TkaAgRx[j] = TkaAgRx[j] + CAg * Fn[i] * (WAg[1] * (SAg[1] - 1) / (SAg[1] * SAg[0])) * ProAgRx[j] * (rhoAg / DensidadBlanco) * self.umaEF[46,i - self.Posici1kev[0]] / (uT[i - self.Posici1kev[0]] + uT[int(Posi2[j]) - self.Posici1kev[0]])

                    #- For Au L -#            
                    for i in range(0, len(TAuDRx)):                            
                        TkaAgRx[j] = TkaAgRx[j] + CAg * TAuDRx[i] * (WAg[1] * (SAg[1] - 1) / SAg[1]) * ProAgRx[j] * (rhoAg / DensidadBlanco) * self.umaEF[46, int(PosiAu[i]) - self.Posici1kev[0]] / (uT[int(PosiAu[i]) - self.Posici1kev[0]] + uT[int(Posi2[j]) - self.Posici1kev[0]])

                ## For Layer L2 ##
                Posi_3=np.array(np.where(self.E>3.524)) ## Ionization Potential of Ag for the L2 layer
                Posici_3=np.array(Posi_3[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L2 layer in Ag

                for j in range(9,13):
                    for i in range(Posici_3[0],Posici_2[0]):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[2]*(SAg[2]-1)/SAg[2])*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[2]*(SAg[2]-1)/(SAg[2]*SAg[1]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[2]*(SAg[2]-1)/(SAg[2]*SAg[1]*SAg[0]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*TAuDRx[i]*(WAg[2]*(SAg[2]-1)/(SAg[2]*SAg[1]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                ## For Layer L3 ##
                Posi_4=np.array(np.where(self.E>3.351)) ## Ionization Potential of Ag for the L3 layer
                Posici_4=np.array(Posi_4[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L3 layer in Ag

                for j in range(13,17):
                    for i in range(Posici_4[0],Posici_3[0]):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[3]*(SAg[3]-1)/SAg[3])*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_3[0],Posici_2[0]):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[3]*(SAg[3]-1)/(SAg[3]*SAg[2]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[3]*(SAg[3]-1)/(SAg[3]*SAg[2]*SAg[1]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        
                    for i in range(Posici_1[0],len(self.E)):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*Fn[i]*(WAg[3]*(SAg[3]-1)/(SAg[3]*SAg[2]*SAg[1]*SAg[0]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        TkaAgRx[j]=TkaAgRx[j]+CAg*TAuDRx[i]*(WAg[3]*(SAg[3]-1)/(SAg[3]*SAg[2]*SAg[1]))*ProAgRx[j]*(rhoAg/DensidadBlanco)*self.umaEF[46,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProAgE)):
                    TkaAgRx[i] = TkaAgRx[i]*np.exp(-self.CoefAire(ProAgE[i])*DensidadAire*d)
                    

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProAgE)):
                    TkaAgRx[i] = round(TkaAgRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProAgE[i])*Fact_G[45])

                TRxEscapeAgka, TRxEscapeAgkb = PicosDe.EscapeSi(TkaAgRx, ProAgE, self.E)

                TkaAgRx = TkaAgRx - (TRxEscapeAgka + TRxEscapeAgkb)
       
                ## X-ray scape for K of Si
                                
                FTAgSika, FTAgSikb = PicosDe.EscapeModelado(TRxEscapeAgka, TRxEscapeAgkb, self.Ancho,
                                                            ProAgE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTAgSika + FTAgSikb

                #-----------------------------------------------------------------------------------------------------------------#

                                    
                AnchAg=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProAgE)
                SigAg=AnchAg/(2*np.sqrt(2*np.log(2)))

                FAg=np.zeros([len(ProAgE),len(self.E)])
                FTAg=np.zeros(len(self.E))
                
                for i in range(0,len(ProAgE)):
                    FAg[i,:]=round(TkaAgRx[i])*Distribucion.GaussLorentz(self.E, SigAg[i], ProAgE[i])

                    FTAg=FTAg+FAg[i,:]

                self.TkaAgRx = TkaAgRx

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #---------------------------------------------          For Barium         ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                CBa=Conc[55]
                rhoBa=3.5 #g/cm3
                
                ProBa=loadtxt('data\ProBa.txt') # Probability that a specific type of X-ray will be generated                      
                ProBaE=ProBa[:,0]
                ProBaRx=ProBa[:,1]

                ProBaQx=loadtxt('data\DatBa.txt') # Energy jump probability and X-ray efficiency

                SBa=ProBaQx[0,:]
                WBa=ProBaQx[1,:]

                Posi2=np.zeros(len(ProBaE))
                for i in range(0,len(ProBaE)):
                    Posi22=np.array(np.where(self.E>ProBaE[i]))
                    Posic=np.array(Posi22[0])
                    Posi2[i]=np.array(Posic[0])

                TkaBaRx=np.zeros(len(Posi2))

                ## For Layer L1 ##
                Posi_1=np.array(np.where(self.E>5.987)) ## Ionization Potential of Ba for the L1 layer                
                Posici_1=np.array(Posi_1[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L1 layer in Ba

                for j in range(0,5):                        
                    for i in range(Posici_1[0], len(self.E)):
                        TkaBaRx[j]=TkaBaRx[j]+CBa*Fn[i]*(WBa[0]*(SBa[0]-1)/SBa[0])*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
          
                    #- For Au L -#
                
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 5.987):                        
                            TkaBaRx[j]=TkaBaRx[j]+CBa*TAuDRx[i]*(WBa[0]*(SBa[0]-1)/SBa[0])*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                ## For Layer L2 ##
                Posi_2=np.array(np.where(self.E>5.624)) ## Ionization Potential of Ba for the L2 layer
                Posici_2=np.array(Posi_2[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L2 layer in Ba

                for j in range(5,9):
                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaBaRx[j]=TkaBaRx[j]+CBa*Fn[i]*(WBa[1]*(SBa[1]-1)/SBa[1])*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TkaBaRx[j]=TkaBaRx[j]+CBa*Fn[i]*(WBa[1]*(SBa[1]-1)/(SBa[1]*SBa[0]))*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 5.624): 
                            TkaBaRx[j]=TkaBaRx[j]+CBa*TAuDRx[i]*(WBa[1]*(SBa[1]-1)/(SBa[1]*SBa[0]))*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                ## For Layer L3 ##
                Posi_3=np.array(np.where(self.E>5.247)) ## Ionization Potential of Pb for the L3 layer
                Posici_3=np.array(Posi_3[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L3 layer in Pb

                for j in range(9,15):                    
                    for i in range(Posici_3[0],Posici_2[0]):
                        TkaBaRx[j]=TkaBaRx[j]+CBa*Fn[i]*(WBa[2]*(SBa[2]-1)/SBa[2])*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaBaRx[j]=TkaBaRx[j]+CBa*Fn[i]*(WBa[2]*(SBa[2]-1)/(SBa[2]*SBa[1]))*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        
                    for i in range(Posici_1[0],len(self.E)):
                        TkaBaRx[j]=TkaBaRx[j]+CBa*Fn[i]*(WBa[2]*(SBa[2]-1)/(SBa[2]*SBa[1]*SBa[0]))*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 5.247):
                            TkaBaRx[j]=TkaBaRx[j]+CBa*TAuDRx[i]*(WBa[2]*(SBa[2]-1)/(SBa[2]*SBa[1]*SBa[0]))*ProBaRx[j]*(rhoBa/DensidadBlanco)*self.umaEF[55,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProBaE)):
                    TkaBaRx[i] = TkaBaRx[i]*np.exp(-self.CoefAire(ProBaE[i])*DensidadAire*d)


                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProBaE)):
                    TkaBaRx[i] = round(TkaBaRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProBaE[i])*Fact_G[49])

                TRxEscapeBaka, TRxEscapeBakb = PicosDe.EscapeSi(TkaBaRx, ProBaE, self.E)

                TkaBaRx = TkaBaRx - (TRxEscapeBaka + TRxEscapeBakb)

                ## X-ray scape for K of Si
                                
                FTBaSika, FTBaSikb = PicosDe.EscapeModelado(TRxEscapeBaka, TRxEscapeBakb, self.Ancho,
                                                            ProBaE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTBaSika + FTBaSikb

                #-----------------------------------------------------------------------------------------------------------------#
     
                AnchBa=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProBaE)
                SigBa=AnchBa/(2*np.sqrt(2*np.log(2)))

                FBa=np.zeros([len(ProBaE),len(self.E)])
                FTBa=np.zeros(len(self.E))
                                    
                
                for i in range(0,len(ProBaE)):
                    FBa[i,:]=round(TkaBaRx[i])*Distribucion.GaussLorentz(self.E, SigBa[i], ProBaE[i])

                    FTBa=FTBa+FBa[i,:]

                self.TkaBaRx = TkaBaRx

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Lead         -----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 
                        
                CPb=Conc[81]
                rhoPb=11.34 #g/cm3
                
                ProPb=loadtxt('data\ProPb.txt') # Probability that a specific type of X-ray will be generated                      
                ProPbE=ProPb[:,0]
                ProPbRx=ProPb[:,1]

                ProPbQx=loadtxt('data\DatPb.txt') # Energy jump probability and X-ray efficiency

                SPb=ProPbQx[0,:]
                WPb=ProPbQx[1,:]
                
                Posi2=np.zeros(len(ProPbE))
                for i in range(0,len(ProPbE)):
                    Posi22=np.array(np.where(self.E>ProPbE[i]))
                    Posic=np.array(Posi22[0])
                    Posi2[i]=np.array(Posic[0])

                TkaPbRx=np.zeros(len(Posi2))

                ## For Layer L1 ##
                Posi_1=np.array(np.where(self.E>15.86)) ## Ionization Potential of Pb for the L1 layer                
                Posici_1=np.array(Posi_1[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L1 layer in Pb

                for j in range(0,9):                        
                    for i in range(Posici_1[0], len(self.E)):
                        TkaPbRx[j]=TkaPbRx[j]+CPb*Fn[i]*(WPb[0]*(SPb[0]-1)/SPb[0])*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
          
                    #- For Au L -#
                
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 15.86):                        
                            TkaPbRx[j]=TkaPbRx[j]+CPb*TAuDRx[i]*(WPb[0]*(SPb[0]-1)/SPb[0])*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                
                ## For Layer L2 ##
                Posi_2=np.array(np.where(self.E>15.198)) ## Ionization Potential of Pb for the L2 layer
                Posici_2=np.array(Posi_2[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L2 layer in Pb

                for j in range(9,16):
                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaPbRx[j]=TkaPbRx[j]+CPb*Fn[i]*(WPb[1]*(SPb[1]-1)/SPb[1])*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TkaPbRx[j]=TkaPbRx[j]+CPb*Fn[i]*(WPb[1]*(SPb[1]-1)/(SPb[1]*SPb[0]))*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 15.198): 
                            TkaPbRx[j]=TkaPbRx[j]+CPb*TAuDRx[i]*(WPb[1]*(SPb[1]-1)/SPb[1])*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                ## For Layer L3 ##
                Posi_3=np.array(np.where(self.E>13.035)) ## Ionization Potential of Pb for the L3 layer
                Posici_3=np.array(Posi_3[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L3 layer in Pb

                for j in range(16,24):                    
                    for i in range(Posici_3[0],Posici_2[0]):
                        TkaPbRx[j]=TkaPbRx[j]+CPb*Fn[i]*(WPb[2]*(SPb[2]-1)/SPb[2])*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaPbRx[j]=TkaPbRx[j]+CPb*Fn[i]*(WPb[2]*(SPb[2]-1)/(SPb[2]*SPb[1]))*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        
                    for i in range(Posici_1[0],len(self.E)):
                        TkaPbRx[j]=TkaPbRx[j]+CPb*Fn[i]*(WPb[2]*(SPb[2]-1)/(SPb[2]*SPb[1]*SPb[0]))*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 13.035):
                            TkaPbRx[j]=TkaPbRx[j]+CPb*TAuDRx[i]*(WPb[2]*(SPb[2]-1)/SPb[2])*ProPbRx[j]*(rhoPb/DensidadBlanco)*self.umaEF[81,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProPbE)):
                    TkaPbRx[i] = TkaPbRx[i]*np.exp(-self.CoefAire(ProPbE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProPbE)):
                    TkaPbRx[i] = round(TkaPbRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProPbE[i])*Fact_G[80])

                TRxEscapePbka, TRxEscapePbkb = PicosDe.EscapeSi(TkaPbRx, ProPbE, self.E)

                TkaPbRx = TkaPbRx - (TRxEscapePbka + TRxEscapePbkb)

                ## X-ray scape for K of Si
                                
                FTPbSika, FTPbSikb = PicosDe.EscapeModelado(TRxEscapePbka, TRxEscapePbkb, self.Ancho,
                                                            ProPbE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTPbSika + FTPbSikb

                #-----------------------------------------------------------------------------------------------------------------#
                    
                AnchPb=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProPbE)
                SigPb=AnchPb/(2*np.sqrt(2*np.log(2)))

                FPb=np.zeros([len(ProPbE),len(self.E)])
                FTPb=np.zeros(len(self.E))
                                    
                
                for i in range(0,len(ProPbE)):
                    FPb[i,:]=round(TkaPbRx[i])*Distribucion.GaussLorentz(self.E, SigPb[i], ProPbE[i])

                    FTPb=FTPb+FPb[i,:]

                self.TkaPbRx = TkaPbRx

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #--------------------------------------------          For Uranium          ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#                    
                
                CU=Conc[91]
                rhoU=19.05 #g/cm3

                ProU=loadtxt('data\ProU.txt') # Probability that a specific type of X-ray will be generated                    
                ProUE=ProU[:,0]
                ProURx=ProU[:,1]

                ProUQx=loadtxt('data\DatU.txt') # Energy jump probability and X-ray efficiency

                SU=ProUQx[0,:]
                WU=ProUQx[1,:]

                Posi2=np.zeros(len(ProUE))
                for i in range(0,len(ProUE)):
                    Posi22=np.array(np.where(self.E>ProUE[i]))
                    Posic=np.array(Posi22[0])
                    Posi2[i]=np.array(Posic[0])

                TkaURx=np.zeros(len(Posi2))

                ## For Layer L1 ##
                Posi_1=np.array(np.where(self.E>21.756)) ## Ionization Potential of U for the L1 layer                
                Posici_1=np.array(Posi_1[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L1 layer in U

                for j in range(0,10):                        
                    for i in range(Posici_1[0], len(self.E)):
                        TkaURx[j]=TkaURx[j]+CU*Fn[i]*(WU[0]*(SU[0]-1)/SU[0])*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
          
                    #- For Au L -#
                
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 21.756):                        
                            TkaURx[j]=TkaURx[j]+CU*TAuDRx[i]*(WU[0]*(SU[0]-1)/SU[0])*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                ## For Layer L2 ##
                Posi_2=np.array(np.where(self.E>20.947)) ## Ionization Potential of U for the L2 layer
                Posici_2=np.array(Posi_2[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L2 layer in U

                for j in range(10,17):
                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaURx[j]=TkaURx[j]+CU*Fn[i]*(WU[1]*(SU[1]-1)/SU[1])*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TkaURx[j]=TkaURx[j]+CU*Fn[i]*(WU[1]*(SU[1]-1)/(SU[1]*SU[0]))*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 20.947): 
                            TkaURx[j]=TkaURx[j]+CU*TAuDRx[i]*(WU[1]*(SU[1]-1)/SU[1])*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                ## For Layer L3 ##
                Posi_3=np.array(np.where(self.E>17.167)) ## Ionization Potential of U for the L3 layer
                Posici_3=np.array(Posi_3[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L3 layer in U

                for j in range(17,25):                    
                    for i in range(Posici_3[0],Posici_2[0]):
                        TkaURx[j]=TkaURx[j]+CU*Fn[i]*(WU[2]*(SU[2]-1)/SU[2])*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TkaURx[j]=TkaURx[j]+CU*Fn[i]*(WU[2]*(SU[2]-1)/(SU[2]*SU[1]))*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        
                    for i in range(Posici_1[0],len(self.E)):
                        TkaURx[j]=TkaURx[j]+CU*Fn[i]*(WU[2]*(SU[2]-1)/(SU[2]*SU[1]*SU[0]))*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 17.167):
                            TkaURx[j]=TkaURx[j]+CU*TAuDRx[i]*(WU[2]*(SU[2]-1)/SU[2])*ProURx[j]*(rhoU/DensidadBlanco)*self.umaEF[91,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
                        
                ## X-ray attenuation due to air ##

                for i in range(0, len(ProUE)):
                    TkaURx[i] = TkaURx[i]*np.exp(-self.CoefAire(ProUE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProUE)):
                    TkaURx[i] = round(TkaURx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProUE[i])*Fact_G[90])

                TRxEscapeUka, TRxEscapeUkb = PicosDe.EscapeSi(TkaURx, ProUE, self.E)

                TkaURx = TkaURx - (TRxEscapeUka + TRxEscapeUkb)

                ## X-ray scape for K of Si
                                
                FTUSika, FTUSikb = PicosDe.EscapeModelado(TRxEscapeUka, TRxEscapeUkb, self.Ancho,
                                                            ProUE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTUSika + FTUSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchU=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProUE)
                SigU=AnchU/(2*np.sqrt(2*np.log(2)))

                FU=np.zeros([len(ProUE),len(self.E)])
                FTU=np.zeros(len(self.E))
                                    
                
                for i in range(0,len(ProUE)):
                    FU[i,:]=round(TkaURx[i])*Distribucion.GaussLorentz(self.E, SigU[i], ProUE[i])

                    FTU=FTU+FU[i,:]

                self.TkaURx = TkaURx


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Aluminum        -------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                
                CAl = Conc[12]
                rhoAl = 2.72 #g/cm3
                
                ProAl = loadtxt('data\ProAl.txt') # Probability that a specific type of X-ray will be generated                     
                ProAlE = ProAl[:,0]
                ProAlRx = ProAl[:,1]

                
                ProAlQx = loadtxt('data\DatAl.txt') # Energy jump probability and X-ray efficiency

                SAl = ProAlQx[0,:]
                WAl = ProAlQx[1,:]

                ## Rx-K Production ##

                TkaAlRx = Prd.ProductionRxK(1.56, CAl, rhoAl,
                                            ProAl, ProAlQx, DensidadBlanco,
                                            self.umaEF[12,:], uT, Fn,
                                            self.E, self.Posici1kev[0])

                                        
                ## Au L contribution ##
                        
                FTAlDAu = Dp.ProductionAuL(1.56, CAl, rhoAl,
                                           ProAl, ProAlQx, DensidadBlanco,
                                           self.umaEF[12,:], uT, ProAuE[5:29],
                                           TAuDRx, self.E, self.Posici1kev[0])


                TkaAlRx = TkaAlRx + FTAlDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProAlE)):
                    TkaAlRx[i] = TkaAlRx[i] * np.exp(-self.CoefAire(ProAlE[i]) * DensidadAire * d)

                #-----------------------------------------------------------------------------------------------------------------#
                # Compton Scatering 
                ProAlE_In = np.zeros(len(ProAlE))
                for i in range(0, len(ProAlE_In)):
                    ProAlE_In[i] = In.EnergiaDifraccion(ProAlE[i], 90)
                

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProAlE)):
                    TkaAlRx[i] = round(TkaAlRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProAlE[i])*Fact_G[11])

                TRxEscapeAlka, TRxEscapeAlkb = PicosDe.EscapeSi(TkaAlRx, ProAlE, self.E)

                
                TkaAlRx = TkaAlRx - (TRxEscapeAlka + TRxEscapeAlkb)

                ## X-ray scape for K of Si

                FTAlSika, FTAlSikb = PicosDe.EscapeModelado(TRxEscapeAlka, TRxEscapeAlkb, self.Ancho,
                                                            ProAlE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTAlSika + FTAlSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchAl = Minimos.Cuadrados(self.Ancho[:,0], self.Ancho[:,1], 1, ProAlE)
                SigAl = AnchAl / (2 * np.sqrt(2 * np.log(2)))

                FAl = np.zeros([len(ProAlE), len(self.E)])
                FTAl = np.zeros(len(self.E))
            
                for i in range(0, len(ProAlE)):
                    FAl[i,:] = round(TkaAlRx[i]) * Distribucion.GaussLorentz(self.E, SigAl[i], ProAlE[i])

                    FTAl = FTAl + FAl[i, :]

                self.TkaAlRx = TkaAlRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_AlRx = self.TkaAlRx / self.Real_Time

                ProAlE_Sum, TkaAlRx_Sum = PicosDe.Suma(ProAlE, Tao, Tasa_AlRx)
                                
                FTAlSum = PicosDe.SumaModelado(ProAlE_Sum, self.Ancho, TkaAlRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTAlSum

                #-----------------------------------------------------------------------------------------------------------------#
                
                

                

                

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Silicon        -------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CSi=Conc[13]
                rhoSi=2.33 #g/cm3
                
                ProSi=loadtxt('data\ProSi.txt') # Probability that a specific type of X-ray will be generated                     
                ProSiE=ProSi[:,0]
                ProSiRx=ProSi[:,1]

                ProSiQx=loadtxt('data\DatSi.txt') # Energy jump probability and X-ray efficiency

                SSi=ProSiQx[0,:]
                WSi=ProSiQx[1,:]

                ## Rx-K Production ##

                TkaSiRx = Prd.ProductionRxK(1.839, CSi, rhoSi,
                                ProSi, ProSiQx, DensidadBlanco,
                                self.umaEF[13,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTSiDAu = Dp.ProductionAuL(1.839, CSi, rhoSi,
                                 ProSi, ProSiQx, DensidadBlanco,
                                 self.umaEF[13,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaSiRx = TkaSiRx + FTSiDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProSiE)):
                    TkaSiRx[i] = TkaSiRx[i]*np.exp(-self.CoefAire(ProSiE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProSiE)):
                    TkaSiRx[i] = round(TkaSiRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProSiE[i])*Fact_G[12])

                TRxEscapeSika, TRxEscapeSikb = PicosDe.EscapeSi(TkaSiRx, ProSiE, self.E)

                TkaSiRx = TkaSiRx - (TRxEscapeSika + TRxEscapeSikb)

                ## X-ray scape for K of Si
                                
                FTSiSika, FTSiSikb = PicosDe.EscapeModelado(TRxEscapeSika, TRxEscapeSikb, self.Ancho,
                                                            ProSiE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTSiSika + FTSiSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchSi=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProSiE)
                SigSi=AnchSi/(2*np.sqrt(2*np.log(2)))

                FSi=np.zeros([len(ProSiE),len(self.E)])
                FTSi=np.zeros(len(self.E))
                
                for i in range(0,len(ProSiE)):
                    FSi[i,:]=round(TkaSiRx[i])*Distribucion.GaussLorentz(self.E, SigSi[i], ProSiE[i])

                    FTSi=FTSi+FSi[i,:]

                self.TkaSiRx = TkaSiRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_SiRx = self.TkaSiRx / self.Real_Time

                ProSiE_Sum, TkaSiRx_Sum = PicosDe.Suma(ProSiE, Tao, Tasa_SiRx)
                                
                FTSiSum = PicosDe.SumaModelado(ProSiE_Sum, self.Ancho, TkaSiRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTSiSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Phosphorus        ------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CP=Conc[14]
                rhoP=1.82 #g/cm3
                
                ProP=loadtxt('data\ProP.txt') # Probability that a specific type of X-ray will be generated                     
                ProPE=ProP[:,0]
                ProPRx=ProP[:,1]

                ProPQx=loadtxt('data\DatP.txt') # Energy jump probability and X-ray efficiency

                SP=ProPQx[0,:]
                WP=ProPQx[1,:]

                ## Rx-K Production ##

                TkaPRx = Prd.ProductionRxK(2.149, CP, rhoP,
                                ProP, ProPQx, DensidadBlanco,
                                self.umaEF[14,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTPDAu = Dp.ProductionAuL(2.149, CP, rhoP,
                                 ProP, ProPQx, DensidadBlanco,
                                 self.umaEF[14,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaPRx = TkaPRx + FTPDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProPE)):
                    TkaPRx[i] = TkaPRx[i]*np.exp(-self.CoefAire(ProPE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProPE)):
                    TkaPRx[i] = round(TkaPRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProPE[i])*Fact_G[13])

                TRxEscapePka, TRxEscapePkb = PicosDe.EscapeSi(TkaPRx, ProPE, self.E)

                TkaPRx = TkaPRx - (TRxEscapePka + TRxEscapePkb)

                ## X-ray scape for K of Si
                                
                FTPSika, FTPSikb = PicosDe.EscapeModelado(TRxEscapePka, TRxEscapePkb, self.Ancho,
                                                            ProPE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTPSika + FTPSikb

                #-----------------------------------------------------------------------------------------------------------------#

            
                AnchP=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProPE)
                SigP=AnchP/(2*np.sqrt(2*np.log(2)))

                FP=np.zeros([len(ProPE),len(self.E)])
                FTP=np.zeros(len(self.E))
                
                for i in range(0,len(ProPE)):
                    FP[i,:]=round(TkaPRx[i])*Distribucion.GaussLorentz(self.E, SigP[i], ProPE[i])

                    FTP=FTP+FP[i,:]

                self.TkaPRx = TkaPRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_PRx = self.TkaPRx / self.Real_Time

                ProPE_Sum, TkaPRx_Sum = PicosDe.Suma(ProPE, Tao, Tasa_PRx)
                                
                FTPSum = PicosDe.SumaModelado(ProPE_Sum, self.Ancho, TkaPRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTPSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Sulfur        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CS=Conc[15]
                rhoS=2.0 #g/cm3
                
                ProS=loadtxt('data\ProS.txt') # Probability that a specific type of X-ray will be generated                     
                ProSE=ProS[:,0]
                ProSRx=ProS[:,1]

                ProSQx=loadtxt('data\DatS.txt') # Energy jump probability and X-ray efficiency

                SS=ProSQx[0,:]
                WS=ProSQx[1,:]

                ## Rx-K Production ##

                TkaSRx = Prd.ProductionRxK(2.472, CS, rhoS,  
                                ProS, ProSQx, DensidadBlanco,
                                self.umaEF[15,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTSDAu = Dp.ProductionAuL(2.472, CS, rhoS,
                                 ProS, ProSQx, DensidadBlanco,
                                 self.umaEF[15,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaSRx = TkaSRx + FTSDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProSE)):
                    TkaSRx[i] = TkaSRx[i]*np.exp(-self.CoefAire(ProSE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProSE)):
                    TkaSRx[i] = round(TkaSRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProSE[i])*Fact_G[14])

                TRxEscapeSka, TRxEscapeSkb = PicosDe.EscapeSi(TkaSRx, ProSE, self.E)

                TkaSRx = TkaSRx - (TRxEscapeSka + TRxEscapeSkb)

                ## X-ray scape for K of Si
                                
                FTSSika, FTSSikb = PicosDe.EscapeModelado(TRxEscapeSka, TRxEscapeSkb, self.Ancho,
                                                            ProSE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTSSika + FTSSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchS=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProSE)
                SigS=AnchS/(2*np.sqrt(2*np.log(2)))

                FS=np.zeros([len(ProSE),len(self.E)])
                FTS=np.zeros(len(self.E))
                
                for i in range(0,len(ProSE)):
                    FS[i,:]=round(TkaSRx[i])*Distribucion.GaussLorentz(self.E, SigS[i], ProSE[i])

                    FTS=FTS+FS[i,:]

                self.TkaSRx = TkaSRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_SRx = self.TkaSRx / self.Real_Time

                ProSE_Sum, TkaSRx_Sum = PicosDe.Suma(ProSE, Tao, Tasa_SRx)
                                
                FTSSum = PicosDe.SumaModelado(ProSE_Sum, self.Ancho, TkaSRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTSSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Chlorine        -------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CCl=Conc[16]
                rhoCl=1.56 #g/cm3
                
                ProCl=loadtxt('data\ProCl.txt') # Probability that a specific type of X-ray will be generated                     
                ProClE=ProCl[:,0]
                ProClRx=ProCl[:,1]

                ProClQx=loadtxt('data\DatCl.txt') # Energy jump probability and X-ray efficiency

                SCl=ProClQx[0,:]
                WCl=ProClQx[1,:]

                ## Rx-K Production ##

                TkaClRx = Prd.ProductionRxK(2.822, CCl, rhoCl,  
                                ProCl, ProClQx, DensidadBlanco,
                                self.umaEF[16,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTClDAu = Dp.ProductionAuL(2.822, CCl, rhoCl,
                                 ProCl, ProClQx, DensidadBlanco,
                                 self.umaEF[16,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaClRx = TkaClRx + FTClDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProClE)):
                    TkaClRx[i] = TkaClRx[i]*np.exp(-self.CoefAire(ProClE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProClE)):
                    TkaClRx[i] = round(TkaClRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProClE[i])*Fact_G[15])

                TRxEscapeClka, TRxEscapeClkb = PicosDe.EscapeSi(TkaClRx, ProClE, self.E)

                TkaClRx = TkaClRx - (TRxEscapeClka + TRxEscapeClkb)

                ## X-ray scape for K of Si
                                
                FTClSika, FTClSikb = PicosDe.EscapeModelado(TRxEscapeClka, TRxEscapeClkb, self.Ancho,
                                                            ProClE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTClSika + FTClSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchCl=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProClE)
                SigCl=AnchCl/(2*np.sqrt(2*np.log(2)))

                FCl=np.zeros([len(ProClE),len(self.E)])
                FTCl=np.zeros(len(self.E))
                
                for i in range(0,len(ProClE)):
                    FCl[i,:]=round(TkaClRx[i])*Distribucion.GaussLorentz(self.E, SigCl[i], ProClE[i])

                    FTCl=FTCl+FCl[i,:]

                self.TkaClRx = TkaClRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_ClRx = self.TkaClRx / self.Real_Time

                ProClE_Sum, TkaClRx_Sum = PicosDe.Suma(ProClE, Tao, Tasa_ClRx)
                                
                FTClSum = PicosDe.SumaModelado(ProClE_Sum, self.Ancho, TkaClRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTClSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-------------------------------------------------          For Argon        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 
                '''try:
                    #29 para el filtro del detector
                    #0.009 para otros
                    CAr=0.009
                    rhoAr=0.001 #g/cm3
                    
                    ProAr=loadtxt('data\ProAr.txt') # Probability that a specific type of X-ray will be generated                     
                    ProArE=ProAr[:,0]
                    ProArRx=ProAr[:,1]

                    
                    ProArQx=loadtxt('data\DatAr.txt') # Energy jump probability and X-ray efficiency

                    SAr=ProArQx[0,:]
                    WAr=ProArQx[1,:]

                    ## Rx-K Production ##

                    TkaArRx = Prd.ProductionRxK(3.202, CAr, rhoAr,
                                                ProAr, ProArQx, DensidadBlanco,
                                                self.umaEF[17,:], uT, Fn,
                                                self.E, self.Posici1kev[0])

                    
                    ## X-ray attenuation due to air ##

                    for i in range(0, len(ProArE)):
                        TkaArRx[i] = TkaArRx[i]*np.exp(-self.CoefAire(ProArE[i])*DensidadAire*d)

                    #-----------------------------------------------------------------------------------------------------------------#
                    ## X-ray in scape

                    for i in range(0,len(ProArE)):
                        TkaArRx[i] = round(TkaArRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProArE[i]))


                    AnchAr = Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProArE)
                    SigAr = AnchAr/(2*np.sqrt(2*np.log(2)))

                    FAr = np.zeros([len(ProArE),len(self.E)])
                    FTAr = np.zeros(len(self.E))
                    
                    for i in range(0,len(ProArE)):
                        FAr[i,:]=round(TkaArRx[i])*Distribucion.GaussLorentz(self.E, SigAr[i], ProArE[i])

                        FTAr=FTAr+FAr[i,:]

                    self.TkaArRx = TkaArRx


                    #-----------------------------------------------------------------------------------------------------------------#
                    ## X-ray in sum

                    Tasa_ArRx = self.TkaArRx / self.Real_Time

                    ProArE_Sum, TkaArRx_Sum = PicosDe.Suma(ProArE, Tao, Tasa_ArRx)
                                    
                    FTArSum = PicosDe.SumaModelado(ProArE_Sum, self.Ancho, TkaArRx_Sum, self.E, self.Real_Time)

                    ## Representation

                    FTSuma = FTSuma + FTArSum

                    #-----------------------------------------------------------------------------------------------------------------#

                except Exception as e:
                    print(e)'''
                
                try:
                    if CAg == 0 :
                        ProAr = loadtxt('data\ProAr.txt') # Probability that a specific type of X-ray will be generated                     
                        ProArE = ProAr[:,0]
                        ProArRx = ProAr[:,1]

                        En_Min_Ar_ka = ProArE[0] / 1.05 #------ ROI
                        En_Max_Ar_ka = ProArE[0] * 1.05 #------ ROI

                        PosiMin = np.array(np.where(self.E > En_Min_Ar_ka))
                        PosiMin = np.array(PosiMin[0])
                        PosiMin = int(np.array(PosiMin[0]))

                        PosiMax = np.array(np.where(self.E > En_Max_Ar_ka))
                        PosiMax = np.array(PosiMax[0])
                        PosiMax = int(np.array(PosiMax[0]))

                        RxArka = 0
                        for i in range(PosiMin, PosiMax + 1):
                            RxArka = RxArka + self.ploty[i]
                        
                        RxArkb = RxArka * ProArRx[1]

                        TkaArRx = [RxArka,  RxArkb]
                        

                        AnchAr = Minimos.Cuadrados(self.Ancho[:,0], self.Ancho[:,1], 1, ProArE)
                        SigAr = AnchAr / (2 * np.sqrt(2 * np.log(2)))

                        FAr = np.zeros([len(ProArE), len(self.E)])
                        FTAr = np.zeros(len(self.E))
                        
                        for i in range(0, len(ProArE)):
                            FAr[i, :] = round(TkaArRx[i]) * Distribucion.GaussLorentz(self.E, SigAr[i], ProArE[i])

                            FTAr = FTAr + FAr[i,:]

                        self.TkaArRx = TkaArRx

                    else:
                        FTAr = np.zeros(len(self.E))

                except Exception as e:
                    print(e)
                

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Potassium        -------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CK=Conc[18]
                rhoK=0.862 #g/cm3
                
                ProK=loadtxt('data\ProK.txt') # Probability that a specific type of X-ray will be generated                     
                ProKE=ProK[:,0]
                ProKRx=ProK[:,1]

                ProKQx=loadtxt('data\DatK.txt') # Energy jump probability and X-ray efficiency

                SK=ProKQx[0,:]
                WK=ProKQx[1,:]

                ## Rx-K Production ##

                TkaKRx = Prd.ProductionRxK(3.607, CK, rhoK,  
                                ProK, ProKQx, DensidadBlanco,
                                self.umaEF[18,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTKDAu = Dp.ProductionAuL(3.607, CK, rhoK,
                                 ProK, ProKQx, DensidadBlanco,
                                 self.umaEF[18,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaKRx = TkaKRx + FTKDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProKE)):
                    TkaKRx[i] = TkaKRx[i]*np.exp(-self.CoefAire(ProKE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProKE)):
                    TkaKRx[i] = round(TkaKRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProKE[i])*Fact_G[17])

                TRxEscapeKka, TRxEscapeKkb = PicosDe.EscapeSi(TkaKRx, ProKE, self.E)

                TkaKRx = TkaKRx - (TRxEscapeKka + TRxEscapeKkb)

                ## X-ray scape for K of Si
                                
                FTKSika, FTKSikb = PicosDe.EscapeModelado(TRxEscapeKka, TRxEscapeKkb, self.Ancho,
                                                            ProKE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTKSika + FTKSikb

                #-----------------------------------------------------------------------------------------------------------------#

                
                AnchK=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProKE)
                SigK=AnchK/(2*np.sqrt(2*np.log(2)))

                FK=np.zeros([len(ProKE),len(self.E)])
                FTK=np.zeros(len(self.E))
                
                for i in range(0,len(ProKE)):
                    FK[i,:]=round(TkaKRx[i])*Distribucion.GaussLorentz(self.E, SigK[i], ProKE[i])

                    FTK=FTK+FK[i,:]

                self.TkaKRx = TkaKRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_KRx = self.TkaKRx / self.Real_Time

                ProKE_Sum, TkaKRx_Sum = PicosDe.Suma(ProKE, Tao, Tasa_KRx)
                                
                FTKSum = PicosDe.SumaModelado(ProKE_Sum, self.Ancho, TkaKRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTKSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Calcium        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CCa=Conc[19]
                rhoCa=1.55 #g/cm3
                
                ProCa=loadtxt('data\ProCa.txt') # Probability that a specific type of X-ray will be generated                     
                ProCaE=ProCa[:,0]
                ProCaRx=ProCa[:,1]

                ProCaQx=loadtxt('data\DatCa.txt') # Energy jump probability and X-ray efficiency

                SCa=ProCaQx[0,:]
                WCa=ProCaQx[1,:]

                ## Rx-K Production ##

                TkaCaRx = Prd.ProductionRxK(4.038, CCa, rhoCa,  
                                ProCa, ProCaQx, DensidadBlanco,
                                self.umaEF[19,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTCaDAu = Dp.ProductionAuL(4.038, CCa, rhoCa,
                                 ProCa, ProCaQx, DensidadBlanco,
                                 self.umaEF[19,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaCaRx = TkaCaRx + FTCaDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProCaE)):
                    TkaCaRx[i] = TkaCaRx[i]*np.exp(-self.CoefAire(ProCaE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProCaE)):
                    TkaCaRx[i] = round(TkaCaRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProCaE[i])*Fact_G[18])

                TRxEscapeCaka, TRxEscapeCakb = PicosDe.EscapeSi(TkaCaRx, ProCaE, self.E)

                TkaCaRx = TkaCaRx - (TRxEscapeCaka + TRxEscapeCakb)

                ## X-ray scape for K of Si
                                
                FTCaSika, FTCaSikb = PicosDe.EscapeModelado(TRxEscapeCaka, TRxEscapeCakb, self.Ancho,
                                                            ProCaE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTCaSika + FTCaSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
            
                AnchCa=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProCaE)
                SigCa=AnchCa/(2*np.sqrt(2*np.log(2)))

                FCa=np.zeros([len(ProCaE),len(self.E)])
                FTCa=np.zeros(len(self.E))
                
                for i in range(0,len(ProCaE)):
                    FCa[i,:]=round(TkaCaRx[i])*Distribucion.GaussLorentz(self.E, SigCa[i], ProCaE[i])

                    FTCa=FTCa+FCa[i,:]

                self.TkaCaRx = TkaCaRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_CaRx = self.TkaCaRx / self.Real_Time

                ProCaE_Sum, TkaCaRx_Sum = PicosDe.Suma(ProCaE, Tao, Tasa_CaRx)
                                
                FTCaSum = PicosDe.SumaModelado(ProCaE_Sum, self.Ancho, TkaCaRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTCaSum

                #-----------------------------------------------------------------------------------------------------------------#
                

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Scandium        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CSc=Conc[20]
                rhoSc=2.992 #g/cm3
                
                ProSc=loadtxt('data\ProSc.txt') # Probability that a specific type of X-ray will be generated                     
                ProScE=ProSc[:,0]
                ProScRx=ProSc[:,1]

                ProScQx=loadtxt('data\DatSc.txt') # Energy jump probability and X-ray efficiency

                SSc=ProScQx[0,:]
                WSc=ProScQx[1,:]

                ## Rx-K Production ##

                TkaScRx = Prd.ProductionRxK(4.493, CSc, rhoSc,  
                                ProSc, ProScQx, DensidadBlanco,
                                self.umaEF[20,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTScDAu = Dp.ProductionAuL(4.493, CSc, rhoSc,
                                 ProSc, ProScQx, DensidadBlanco,
                                 self.umaEF[20,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaScRx = TkaScRx + FTScDAu

                ## X-ray attenuation due to air ##
                
                for i in range(0, len(ProScE)):                    
                    TkaScRx[i] = TkaScRx[i]*np.exp(-self.CoefAire(ProScE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProScE)):
                    TkaScRx[i] = round(TkaScRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProScE[i])*Fact_G[19])

                TRxEscapeScka, TRxEscapeSckb = PicosDe.EscapeSi(TkaScRx, ProScE, self.E)

                TkaScRx = TkaScRx - (TRxEscapeScka + TRxEscapeSckb)

                ## X-ray scape for K of Si
                                
                FTScSika, FTScSikb = PicosDe.EscapeModelado(TRxEscapeScka, TRxEscapeSckb, self.Ancho,
                                                            ProScE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTScSika + FTScSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
            
                AnchSc=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProScE)
                SigSc=AnchSc/(2*np.sqrt(2*np.log(2)))

                FSc=np.zeros([len(ProScE),len(self.E)])
                FTSc=np.zeros(len(self.E))
                
                for i in range(0,len(ProScE)):
                    FSc[i,:]=round(TkaScRx[i])*Distribucion.GaussLorentz(self.E, SigSc[i], ProScE[i])

                    FTSc=FTSc+FSc[i,:]

                self.TkaScRx = TkaScRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_ScRx = self.TkaScRx / self.Real_Time

                ProScE_Sum, TkaScRx_Sum = PicosDe.Suma(ProScE, Tao, Tasa_ScRx)
                                
                FTScSum = PicosDe.SumaModelado(ProScE_Sum, self.Ancho, TkaScRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTScSum

                #-----------------------------------------------------------------------------------------------------------------#
                

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Titanium        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CTi=Conc[21]
                rhoTi=4.54 #g/cm3
                
                ProTi=loadtxt('data\ProTi.txt') # Probability that a specific type of X-ray will be generated                     
                ProTiE=ProTi[:,0]
                ProTiRx=ProTi[:,1]

                ProTiQx=loadtxt('data\DatTi.txt') # Energy jump probability and X-ray efficiency

                STi=ProTiQx[0,:]
                WTi=ProTiQx[1,:]

                ## Rx-K Production ##

                TkaTiRx = Prd.ProductionRxK(4.965, CTi, rhoTi,  
                                ProTi, ProTiQx, DensidadBlanco,
                                self.umaEF[21,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTTiDAu = Dp.ProductionAuL(4.965, CTi, rhoTi,
                                 ProTi, ProTiQx, DensidadBlanco,
                                 self.umaEF[21,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaTiRx = TkaTiRx + FTTiDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProTiE)):
                    TkaTiRx[i] = TkaTiRx[i]*np.exp(-self.CoefAire(ProTiE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProTiE)):
                    TkaTiRx[i] = round(TkaTiRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProTiE[i])*Fact_G[20])

                TRxEscapeTika, TRxEscapeTikb = PicosDe.EscapeSi(TkaTiRx, ProTiE, self.E)

                TkaTiRx = TkaTiRx - (TRxEscapeTika + TRxEscapeTikb)

                ## X-ray scape for K of Si
                                
                FTTiSika, FTTiSikb = PicosDe.EscapeModelado(TRxEscapeTika, TRxEscapeTikb, self.Ancho,
                                                            ProTiE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTTiSika + FTTiSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchTi=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProTiE)
                SigTi=AnchTi/(2*np.sqrt(2*np.log(2)))

                FTi=np.zeros([len(ProTiE),len(self.E)])
                FTTi=np.zeros(len(self.E))
                
                for i in range(0,len(ProTiE)):
                    FTi[i,:]=round(TkaTiRx[i])*Distribucion.GaussLorentz(self.E, SigTi[i], ProTiE[i])

                    FTTi=FTTi+FTi[i,:]

                self.TkaTiRx = TkaTiRx
                
                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_TiRx = self.TkaTiRx / self.Real_Time

                ProTiE_Sum, TkaTiRx_Sum = PicosDe.Suma(ProTiE, Tao, Tasa_TiRx)
                                
                FTTiSum = PicosDe.SumaModelado(ProTiE_Sum, self.Ancho, TkaTiRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTTiSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Vanadium        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CV=Conc[22]
                rhoV=6.11 #g/cm3
                
                ProV=loadtxt('data\ProV.txt') # Probability that a specific type of X-ray will be generated                     
                ProVE=ProV[:,0]
                ProVRx=ProV[:,1]

                ProVQx=loadtxt('data\DatV.txt') # Energy jump probability and X-ray efficiency

                SV=ProVQx[0,:]
                WV=ProVQx[1,:]

                ## Rx-K Production ##

                TkaVRx = Prd.ProductionRxK(5.465, CV, rhoV,  
                                ProV, ProVQx, DensidadBlanco,
                                self.umaEF[22,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTVDAu = Dp.ProductionAuL(5.465, CV, rhoV,
                                 ProV, ProVQx, DensidadBlanco,
                                 self.umaEF[22,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaVRx = TkaVRx + FTVDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProVE)):
                    TkaVRx[i] = TkaVRx[i]*np.exp(-self.CoefAire(ProVE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProVE)):
                    TkaVRx[i] = round(TkaVRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProVE[i])*Fact_G[21])

                TRxEscapeVka, TRxEscapeVkb = PicosDe.EscapeSi(TkaVRx, ProVE, self.E)

                TkaVRx = TkaVRx - (TRxEscapeVka + TRxEscapeVkb)

                ## X-ray scape for K of Si
                                
                FTVSika, FTVSikb = PicosDe.EscapeModelado(TRxEscapeVka, TRxEscapeVkb, self.Ancho,
                                                            ProVE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTVSika + FTVSikb

                #-----------------------------------------------------------------------------------------------------------------#

            
                AnchV=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProVE)
                SigV=AnchV/(2*np.sqrt(2*np.log(2)))

                FV=np.zeros([len(ProVE),len(self.E)])
                FTV=np.zeros(len(self.E))
                
                for i in range(0,len(ProVE)):
                    FV[i,:]=round(TkaVRx[i])*Distribucion.GaussLorentz(self.E, SigV[i], ProVE[i])

                    FTV=FTV+FV[i,:]

                self.TkaVRx = TkaVRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_VRx = self.TkaVRx / self.Real_Time

                ProVE_Sum, TkaVRx_Sum = PicosDe.Suma(ProVE, Tao, Tasa_VRx)
                                
                FTVSum = PicosDe.SumaModelado(ProVE_Sum, self.Ancho, TkaVRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTVSum

                #-----------------------------------------------------------------------------------------------------------------#
                

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Chrome        ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CCr=Conc[23]
                rhoCr=7.19 #g/cm3
                
                ProCr=loadtxt('data\ProCr.txt') # Probability that a specific type of X-ray will be generated                     
                ProCrE=ProCr[:,0]
                ProCrRx=ProCr[:,1]

                ProCrQx=loadtxt('data\DatCr.txt') # Energy jump probability and X-ray efficiency

                SCr=ProCrQx[0,:]
                WCr=ProCrQx[1,:]

                ## Rx-K Production ##

                TkaCrRx = Prd.ProductionRxK(5.9889, CCr, rhoCr,  
                                ProCr, ProCrQx, DensidadBlanco,
                                self.umaEF[23,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTCrDAu = Dp.ProductionAuL(5.9889, CCr, rhoCr,
                                 ProCr, ProCrQx, DensidadBlanco,
                                 self.umaEF[23,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaCrRx = TkaCrRx + FTCrDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProCrE)):
                    TkaCrRx[i] = TkaCrRx[i]*np.exp(-self.CoefAire(ProCrE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProCrE)):
                    TkaCrRx[i] = round(TkaCrRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProCrE[i])*Fact_G[22])

                TRxEscapeCrka, TRxEscapeCrkb = PicosDe.EscapeSi(TkaCrRx, ProCrE, self.E)

                TkaCrRx = TkaCrRx - (TRxEscapeCrka + TRxEscapeCrkb)

                ## X-ray scape for K of Si
                                
                FTCrSika, FTCrSikb = PicosDe.EscapeModelado(TRxEscapeCrka, TRxEscapeCrkb, self.Ancho,
                                                            ProCrE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTCrSika + FTCrSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchCr=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProCrE)
                SigCr=AnchCr/(2*np.sqrt(2*np.log(2)))

                FCr=np.zeros([len(ProCrE),len(self.E)])
                FTCr=np.zeros(len(self.E))
                
                for i in range(0,len(ProCrE)):
                    FCr[i,:]=round(TkaCrRx[i])*Distribucion.GaussLorentz(self.E, SigCr[i], ProCrE[i])

                    FTCr=FTCr+FCr[i,:]

                self.TkaCrRx = TkaCrRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_CrRx = self.TkaCrRx / self.Real_Time

                ProCrE_Sum, TkaCrRx_Sum = PicosDe.Suma(ProCrE, Tao, Tasa_CrRx)
                                
                FTCrSum = PicosDe.SumaModelado(ProCrE_Sum, self.Ancho, TkaCrRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTCrSum

                #-----------------------------------------------------------------------------------------------------------------#
                

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #---------------------------------------------          For Manganese        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                CMn=Conc[24]
                rhoMn=7.42 #g/cm3
                
                ProMn=loadtxt('data\ProMn.txt') # Probability that a specific type of X-ray will be generated                     
                ProMnE=ProMn[:,0]
                ProMnRx=ProMn[:,1]

                ProMnQx=loadtxt('data\DatMn.txt') # Energy jump probability and X-ray efficiency

                SMn=ProMnQx[0,:]
                WMn=ProMnQx[1,:]

                ## Rx-K Production ##

                TkaMnRx = Prd.ProductionRxK(6.54, CMn, rhoMn,  
                                ProMn, ProMnQx, DensidadBlanco,
                                self.umaEF[24,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTMnDAu = Dp.ProductionAuL(6.54, CMn, rhoMn,
                                 ProMn, ProMnQx, DensidadBlanco,
                                 self.umaEF[24,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaMnRx = TkaMnRx + FTMnDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProMnE)):
                    TkaMnRx[i] = TkaMnRx[i]*np.exp(-self.CoefAire(ProMnE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProMnE)):
                    TkaMnRx[i] = round(TkaMnRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProMnE[i])*Fact_G[23])

                TRxEscapeMnka, TRxEscapeMnkb = PicosDe.EscapeSi(TkaMnRx, ProMnE, self.E)

                TkaMnRx = TkaMnRx - (TRxEscapeMnka + TRxEscapeMnkb)

                ## X-ray scape for K of Si
                                
                FTMnSika, FTMnSikb = PicosDe.EscapeModelado(TRxEscapeMnka, TRxEscapeMnkb, self.Ancho,
                                                            ProMnE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTMnSika + FTMnSikb

                #-----------------------------------------------------------------------------------------------------------------#
            
                AnchMn=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProMnE)
                SigMn=AnchMn/(2*np.sqrt(2*np.log(2)))

                FMn=np.zeros([len(ProMnE),len(self.E)])
                FTMn=np.zeros(len(self.E))
                
                for i in range(0,len(ProMnE)):
                    FMn[i,:]=round(TkaMnRx[i])*Distribucion.GaussLorentz(self.E, SigMn[i], ProMnE[i])

                    FTMn=FTMn+FMn[i,:]

                self.TkaMnRx = TkaMnRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_MnRx = self.TkaMnRx / self.Real_Time

                ProMnE_Sum, TkaMnRx_Sum = PicosDe.Suma(ProMnE, Tao, Tasa_MnRx)
                                
                FTMnSum = PicosDe.SumaModelado(ProMnE_Sum, self.Ancho, TkaMnRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTMnSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Iron        -----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                        
                CFe=Conc[25]
                rhoFe=7.86 #g/cm3
                
                ProFe=loadtxt('data\ProFe.txt') # Probability that a specific type of X-ray will be generated                     
                ProFeE=ProFe[:,0]
                ProFeRx=ProFe[:,1]

                ProFeQx=loadtxt('data\DatFe.txt') # Energy jump probability and X-ray efficiency

                SFe=ProFeQx[0,:]
                WFe=ProFeQx[1,:]

                ## Rx-K Production ##

                TkaFeRx = Prd.ProductionRxK(7.112, CFe, rhoFe,                                          
                                ProFe, ProFeQx, DensidadBlanco,
                                self.umaEF[25,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTFeDAu = Dp.ProductionAuL(7.112, CFe, rhoFe,
                                 ProFe, ProFeQx, DensidadBlanco,
                                 self.umaEF[25,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaFeRx = TkaFeRx + FTFeDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProFeE)):
                    TkaFeRx[i] = TkaFeRx[i]*np.exp(-self.CoefAire(ProFeE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProFeE)):
                    TkaFeRx[i] = round(TkaFeRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProFeE[i])*Fact_G[24])

                TRxEscapeFeka, TRxEscapeFekb = PicosDe.EscapeSi(TkaFeRx, ProFeE, self.E)

                TkaFeRx = TkaFeRx - (TRxEscapeFeka + TRxEscapeFekb)

                ## X-ray scape for K of Si
                                
                FTFeSika, FTFeSikb = PicosDe.EscapeModelado(TRxEscapeFeka, TRxEscapeFekb, self.Ancho,
                                                            ProFeE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTFeSika + FTFeSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchFe=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProFeE)
                SigFe=AnchFe/(2*np.sqrt(2*np.log(2)))

                FFe=np.zeros([len(ProFeE),len(self.E)])
                FTFe=np.zeros(len(self.E))
                
                for i in range(0,len(ProFeE)):
                    FFe[i,:]=round(TkaFeRx[i])*Distribucion.GaussLorentz(self.E, SigFe[i], ProFeE[i])

                    FTFe=FTFe+FFe[i,:]

                self.TkaFeRx = TkaFeRx           

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_FeRx = self.TkaFeRx / self.Real_Time
                                
                ProFeE_Sum, TkaFeRx_Sum = PicosDe.Suma(ProFeE, Tao, Tasa_FeRx)

                FTFeSum = PicosDe.SumaModelado(ProFeE_Sum, self.Ancho, TkaFeRx_Sum, self.E, self.Real_Time)
                
                ## Representation

                FTSuma = FTSuma + FTFeSum

                #-----------------------------------------------------------------------------------------------------------------#     


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Cobalt        --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------# 

                        
                CCo=Conc[26]
                rhoCo=8.90 #g/cm3
                
                ProCo=loadtxt('data\ProCo.txt') # Probability that a specific type of X-ray will be generated                     
                ProCoE=ProCo[:,0]
                ProCoRx=ProCo[:,1]

                ProCoQx=loadtxt('data\DatCo.txt') # Energy jump probability and X-ray efficiency

                SCo=ProCoQx[0,:]
                WCo=ProCoQx[1,:]

                ## Rx-K Production ##

                TkaCoRx = Prd.ProductionRxK(7.709, CCo, rhoCo,
                                ProCo, ProCoQx, DensidadBlanco,
                                self.umaEF[26,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                            
                                        
                ## Au L contribution ##
                        
                FTCoDAu = Dp.ProductionAuL(7.709, CCo, rhoCo,
                                 ProCo, ProCoQx, DensidadBlanco,
                                 self.umaEF[26,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaCoRx = TkaCoRx + FTCoDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProCoE)):
                    TkaCoRx[i] = TkaCoRx[i]*np.exp(-self.CoefAire(ProCoE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProCoE)):
                    TkaCoRx[i] = round(TkaCoRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProCoE[i])*Fact_G[25])

                TRxEscapeCoka, TRxEscapeCokb = PicosDe.EscapeSi(TkaCoRx, ProCoE, self.E)

                TkaCoRx = TkaCoRx - (TRxEscapeCoka + TRxEscapeCokb)

                ## X-ray scape for K of Si
                                
                FTCoSika, FTCoSikb = PicosDe.EscapeModelado(TRxEscapeCoka, TRxEscapeCokb, self.Ancho,
                                                            ProCoE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTCoSika + FTCoSikb

                #-----------------------------------------------------------------------------------------------------------------#
                        
                AnchCo=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProCoE)
                SigCo=AnchCo/(2*np.sqrt(2*np.log(2)))

                FCo=np.zeros([len(ProCoE),len(self.E)])
                FTCo=np.zeros(len(self.E))
                
                for i in range(0,len(ProCoE)):
                    FCo[i,:]=round(TkaCoRx[i])*Distribucion.GaussLorentz(self.E, SigCo[i], ProCoE[i])

                    FTCo=FTCo+FCo[i,:]

                self.TkaCoRx = TkaCoRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_CoRx = self.TkaCoRx / self.Real_Time

                ProCoE_Sum, TkaCoRx_Sum = PicosDe.Suma(ProCoE, Tao, Tasa_CoRx)
                                
                FTCoSum = PicosDe.SumaModelado(ProCoE_Sum, self.Ancho, TkaCoRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTCoSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Nickel        ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CNi=Conc[27]
                rhoNi=8.90 #g/cm3

                ProNi=loadtxt('data\ProNi.txt') # Probability that a specific type of X-ray will be generated                     
                ProNiE=ProNi[:,0]
                ProNiRx=ProNi[:,1]

                ProNiQx=loadtxt('data\DatNi.txt') # Energy jump probability and X-ray efficiency

                SNi=ProNiQx[0,:]
                WNi=ProNiQx[1,:]

                ## Rx-K Production ##

                TkaNiRx = Prd.ProductionRxK(8.333, CNi, rhoNi,                                          
                                ProNi, ProNiQx, DensidadBlanco,
                                self.umaEF[27,:], uT, Fn,
                                self.E, self.Posici1kev[0])

                
                ## Au L contribution ##
                        
                FTNiDAu = Dp.ProductionAuL(8.333, CNi, rhoNi,
                                 ProNi, ProNiQx, DensidadBlanco,
                                 self.umaEF[27,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaNiRx = TkaNiRx + FTNiDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProNiE)):
                    TkaNiRx[i] = TkaNiRx[i]*np.exp(-self.CoefAire(ProNiE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProNiE)):
                    TkaNiRx[i] = round(TkaNiRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProNiE[i])*Fact_G[26])

                TRxEscapeNika, TRxEscapeNikb = PicosDe.EscapeSi(TkaNiRx, ProNiE, self.E)

                TkaNiRx = TkaNiRx - (TRxEscapeNika + TRxEscapeNikb)

                ## X-ray scape for K of Si
                                
                FTNiSika, FTNiSikb = PicosDe.EscapeModelado(TRxEscapeNika, TRxEscapeNikb, self.Ancho,
                                                            ProNiE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTNiSika + FTNiSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchNi=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProNiE)
                SigNi=AnchNi/(2*np.sqrt(2*np.log(2)))

                FNi=np.zeros([len(ProNiE),len(self.E)])
                FTNi=np.zeros(len(self.E))
                
                for i in range(0,len(ProNiE)):
                    FNi[i,:]=round(TkaNiRx[i])*Distribucion.GaussLorentz(self.E, SigNi[i], ProNiE[i])

                    FTNi=FTNi+FNi[i,:]

                self.TkaNiRx = TkaNiRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_NiRx = self.TkaNiRx / self.Real_Time

                ProNiE_Sum, TkaNiRx_Sum = PicosDe.Suma(ProNiE, Tao, Tasa_NiRx)
                                
                FTNiSum = PicosDe.SumaModelado(ProNiE_Sum, self.Ancho, TkaNiRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTNiSum

                #-----------------------------------------------------------------------------------------------------------------#

                
                    
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Copper        ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CCu=Conc[28]
                rhoCu=8.94 #g/cm3
                
                ProCu=loadtxt('data\ProCu.txt') # Probability that a specific type of X-ray will be generated                     
                ProCuE=ProCu[:,0]
                ProCuRx=ProCu[:,1]


                ProCuQx=loadtxt('data\DatCu.txt') # Energy jump probability and X-ray efficiency

                SCu=ProCuQx[0,:]
                WCu=ProCuQx[1,:]

                ## Rx-K Production ##

                TkaCuRx = Prd.ProductionRxK(8.979, CCu, rhoCu,                                          
                                ProCu, ProCuQx, DensidadBlanco,
                                self.umaEF[28,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                
                ## Au L contribution ##
                        
                FTCuDAu = Dp.ProductionAuL(8.979, CCu, rhoCu,
                                 ProCu, ProCuQx, DensidadBlanco,
                                 self.umaEF[28,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaCuRx = TkaCuRx + FTCuDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProCuE)):
                    TkaCuRx[i] = TkaCuRx[i]*np.exp(-self.CoefAire(ProCuE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProCuE)):
                    TkaCuRx[i] = round(TkaCuRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProCuE[i])*Fact_G[27])

                TRxEscapeCuka, TRxEscapeCukb = PicosDe.EscapeSi(TkaCuRx, ProCuE, self.E)

                TkaCuRx = TkaCuRx - (TRxEscapeCuka + TRxEscapeCukb)

                ## X-ray scape for K of Si
                                
                FTCuSika, FTCuSikb = PicosDe.EscapeModelado(TRxEscapeCuka, TRxEscapeCukb, self.Ancho,
                                                            ProCuE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTCuSika + FTCuSikb

                #-----------------------------------------------------------------------------------------------------------------#

                AnchCu=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProCuE)
                SigCu=AnchCu/(2*np.sqrt(2*np.log(2)))


                FCu=np.zeros([len(ProCuE),len(self.E)])
                FTCu=np.zeros(len(self.E))
                
                for i in range(0,len(ProCuE)):
                    FCu[i,:]=round(TkaCuRx[i])*Distribucion.GaussLorentz(self.E, SigCu[i], ProCuE[i])

                    FTCu=FTCu+FCu[i,:]

                self.TkaCuRx = TkaCuRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_CuRx = self.TkaCuRx / self.Real_Time

                ProCuE_Sum, TkaCuRx_Sum = PicosDe.Suma(ProCuE, Tao, Tasa_CuRx)
                                
                FTCuSum = PicosDe.SumaModelado(ProCuE_Sum, self.Ancho, TkaCuRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTCuSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Zinc       -----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CZn=Conc[29]
                rhoZn=7.14 #g/cm3
                
                ProZn=loadtxt('data\ProZn.txt') # Probability that a specific type of X-ray will be generated                       
                ProZnE=ProZn[:,0]
                ProZnRx=ProZn[:,1]

                ProZnQx=loadtxt('data\DatZn.txt') # Energy jump probability and X-ray efficiency

                SZn=ProZnQx[0,:]
                WZn=ProZnQx[1,:]

                ## Rx-K Production ##

                TkaZnRx = Prd.ProductionRxK(9.659, CZn, rhoZn,                                          
                                ProZn, ProZnQx, DensidadBlanco,
                                self.umaEF[29,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTZnDAu = Dp.ProductionAuL(9.659, CZn, rhoZn,
                                 ProZn, ProZnQx, DensidadBlanco,
                                 self.umaEF[29,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaZnRx = TkaZnRx + FTZnDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProZnE)):
                    TkaZnRx[i] = TkaZnRx[i]*np.exp(-self.CoefAire(ProZnE[i])*DensidadAire*d)
                
                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProZnE)):
                    TkaZnRx[i] = round(TkaZnRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProZnE[i])*Fact_G[28])

                TRxEscapeZnka, TRxEscapeZnkb = PicosDe.EscapeSi(TkaZnRx, ProZnE, self.E)

                TkaZnRx = TkaZnRx - (TRxEscapeZnka + TRxEscapeZnkb)

                ## X-ray scape for K of Si
                                
                FTZnSika, FTZnSikb = PicosDe.EscapeModelado(TRxEscapeZnka, TRxEscapeZnkb, self.Ancho,
                                                            ProZnE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTZnSika + FTZnSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchZn=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProZnE)
                SigZn=AnchZn/(2*np.sqrt(2*np.log(2)))

                FZn=np.zeros([len(ProZnE),len(self.E)])
                FTZn=np.zeros(len(self.E))
                
                for i in range(0,len(ProZnE)):
                    FZn[i,:]=round(TkaZnRx[i])*Distribucion.GaussLorentz(self.E, SigZn[i], ProZnE[i])

                    FTZn=FTZn+FZn[i,:]

                self.TkaZnRx = TkaZnRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_ZnRx = self.TkaZnRx / self.Real_Time

                ProZnE_Sum, TkaZnRx_Sum = PicosDe.Suma(ProZnE, Tao, Tasa_ZnRx)
                                
                FTZnSum = PicosDe.SumaModelado(ProZnE_Sum, self.Ancho, TkaZnRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTZnSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Gallium       ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CGa=Conc[30]
                rhoGa=5.903 #g/cm3
                
                ProGa=loadtxt('data\ProGa.txt') # Probability that a specific type of X-ray will be generated                       
                ProGaE=ProGa[:,0]
                ProGaRx=ProGa[:,1]

                ProGaQx=loadtxt('data\DatGa.txt') # Energy jump probability and X-ray efficiency

                SGa=ProGaQx[0,:]
                WGa=ProGaQx[1,:]

                ## Rx-K Production ##

                TkaGaRx = Prd.ProductionRxK(10.367, CGa, rhoGa, 
                                ProGa, ProGaQx, DensidadBlanco,
                                self.umaEF[30,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTGaDAu = Dp.ProductionAuL(10.367, CGa, rhoGa,
                                 ProGa, ProGaQx, DensidadBlanco,
                                 self.umaEF[30,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaGaRx = TkaGaRx + FTGaDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProGaE)):
                    TkaGaRx[i] = TkaGaRx[i]*np.exp(-self.CoefAire(ProGaE[i])*DensidadAire*d)
                
                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProGaE)):
                    TkaGaRx[i] = round(TkaGaRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProGaE[i])*Fact_G[29])

                TRxEscapeGaka, TRxEscapeGakb = PicosDe.EscapeSi(TkaGaRx, ProGaE, self.E)

                TkaGaRx = TkaGaRx - (TRxEscapeGaka + TRxEscapeGakb)

                ## X-ray scape for K of Si
                                
                FTGaSika, FTGaSikb = PicosDe.EscapeModelado(TRxEscapeGaka, TRxEscapeGakb, self.Ancho,
                                                            ProGaE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTGaSika + FTGaSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchGa=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProGaE)
                SigGa=AnchGa/(2*np.sqrt(2*np.log(2)))

                FGa=np.zeros([len(ProGaE),len(self.E)])
                FTGa=np.zeros(len(self.E))
                
                for i in range(0,len(ProGaE)):
                    FGa[i,:]=round(TkaGaRx[i])*Distribucion.GaussLorentz(self.E, SigGa[i], ProGaE[i])

                    FTGa=FTGa+FGa[i,:]

                self.TkaGaRx = TkaGaRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_GaRx = self.TkaGaRx / self.Real_Time

                ProGaE_Sum, TkaGaRx_Sum = PicosDe.Suma(ProGaE, Tao, Tasa_GaRx)
                                
                FTGaSum = PicosDe.SumaModelado(ProGaE_Sum, self.Ancho, TkaGaRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTGaSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #----------------------------------------------          For Germanium       --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CGe=Conc[31]
                rhoGe=5.323 #g/cm3
                
                ProGe=loadtxt('data\ProGe.txt') # Probability that a specific type of X-ray will be generated                       
                ProGeE=ProGe[:,0]
                ProGeRx=ProGe[:,1]

                ProGeQx=loadtxt('data\DatGe.txt') # Energy jump probability and X-ray efficiency

                SGe=ProGeQx[0,:]
                WGe=ProGeQx[1,:]

                ## Rx-K Production ##

                TkaGeRx = Prd.ProductionRxK(11.104, CGe, rhoGe, 
                                ProGe, ProGeQx, DensidadBlanco,
                                self.umaEF[31,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTGeDAu = Dp.ProductionAuL(11.104, CGe, rhoGe,
                                 ProGe, ProGeQx, DensidadBlanco,
                                 self.umaEF[31,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaGeRx = TkaGeRx + FTGeDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProGeE)):
                    TkaGeRx[i] = TkaGeRx[i]*np.exp(-self.CoefAire(ProGeE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProGeE)):
                    TkaGeRx[i] = round(TkaGeRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProGeE[i])*Fact_G[30])

                TRxEscapeGeka, TRxEscapeGekb = PicosDe.EscapeSi(TkaGeRx, ProGeE, self.E)

                TkaGeRx = TkaGeRx - (TRxEscapeGeka + TRxEscapeGekb)

                ## X-ray scape for K of Si
                                
                FTGeSika, FTGeSikb = PicosDe.EscapeModelado(TRxEscapeGeka, TRxEscapeGekb, self.Ancho,
                                                            ProGeE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTGeSika + FTGeSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchGe=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProGeE)
                SigGe=AnchGe/(2*np.sqrt(2*np.log(2)))

                FGe=np.zeros([len(ProGeE),len(self.E)])
                FTGe=np.zeros(len(self.E))
                
                for i in range(0,len(ProGeE)):
                    FGe[i,:]=round(TkaGeRx[i])*Distribucion.GaussLorentz(self.E, SigGe[i], ProGeE[i])

                    FTGe=FTGe+FGe[i,:]

                self.TkaGeRx = TkaGeRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_GeRx = self.TkaGeRx / self.Real_Time

                ProGeE_Sum, TkaGeRx_Sum = PicosDe.Suma(ProGeE, Tao, Tasa_GeRx)
                                
                FTGeSum = PicosDe.SumaModelado(ProGeE_Sum, self.Ancho, TkaGeRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTGeSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Arsenic      ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CAs=Conc[32]
                rhoAs=5.73 #g/cm3
                
                ProAs=loadtxt('data\ProAs.txt') # Probability that a specific type of X-ray will be generated                       
                ProAsE=ProAs[:,0]
                ProAsRx=ProAs[:,1]

                ProAsQx=loadtxt('data\DatAs.txt') # Energy jump probability and X-ray efficiency

                SAs=ProAsQx[0,:]
                WAs=ProAsQx[1,:]

                ## Rx-K Production ##

                TkaAsRx = Prd.ProductionRxK(11.868, CAs, rhoAs, 
                                ProAs, ProAsQx, DensidadBlanco,
                                self.umaEF[32,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTAsDAu = Dp.ProductionAuL(11.868, CAs, rhoAs,
                                 ProAs, ProAsQx, DensidadBlanco,
                                 self.umaEF[32,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaAsRx = TkaAsRx + FTAsDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProAsE)):
                    TkaAsRx[i] = TkaAsRx[i]*np.exp(-self.CoefAire(ProAsE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProAsE)):
                    TkaAsRx[i] = round(TkaAsRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProAsE[i])*Fact_G[31])

                TRxEscapeAska, TRxEscapeAskb = PicosDe.EscapeSi(TkaAsRx, ProAsE, self.E)

                TkaAsRx = TkaAsRx - (TRxEscapeAska + TRxEscapeAskb)

                ## X-ray scape for K of Si
                                
                FTAsSika, FTAsSikb = PicosDe.EscapeModelado(TRxEscapeAska, TRxEscapeAskb, self.Ancho,
                                                            ProAsE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTAsSika + FTAsSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchAs=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProAsE)
                SigAs=AnchAs/(2*np.sqrt(2*np.log(2)))

                FAs=np.zeros([len(ProAsE),len(self.E)])
                FTAs=np.zeros(len(self.E))
                
                for i in range(0,len(ProAsE)):
                    FAs[i,:]=round(TkaAsRx[i])*Distribucion.GaussLorentz(self.E, SigAs[i], ProAsE[i])

                    FTAs=FTAs+FAs[i,:]

                self.TkaAsRx = TkaAsRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_AsRx = self.TkaAsRx / self.Real_Time

                ProAsE_Sum, TkaAsRx_Sum = PicosDe.Suma(ProAsE, Tao, Tasa_AsRx)
                                
                FTAsSum = PicosDe.SumaModelado(ProAsE_Sum, self.Ancho, TkaAsRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTAsSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Selenium      --------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                        
                CSe=Conc[33]
                rhoSe=4.79 #g/cm3
                
                ProSe=loadtxt('data\ProSe.txt') # Probability that a specific type of X-ray will be generated                       
                ProSeE=ProSe[:,0]
                ProSeRx=ProSe[:,1]

                ProSeQx=loadtxt('data\DatSe.txt') # Energy jump probability and X-ray efficiency

                SSe=ProSeQx[0,:]
                WSe=ProSeQx[1,:]

                ## Rx-K Production ##

                TkaSeRx = Prd.ProductionRxK(12.658, CSe, rhoSe, 
                                ProSe, ProSeQx, DensidadBlanco,
                                self.umaEF[33,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTSeDAu = Dp.ProductionAuL(12.658, CSe, rhoSe,
                                 ProSe, ProSeQx, DensidadBlanco,
                                 self.umaEF[33,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaSeRx = TkaSeRx + FTSeDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProSeE)):
                    TkaSeRx[i] = TkaSeRx[i]*np.exp(-self.CoefAire(ProSeE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProSeE)):
                    TkaSeRx[i] = round(TkaSeRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProSeE[i])*Fact_G[32])

                TRxEscapeSeka, TRxEscapeSekb = PicosDe.EscapeSi(TkaSeRx, ProSeE, self.E)

                TkaSeRx = TkaSeRx - (TRxEscapeSeka + TRxEscapeSekb)

                ## X-ray scape for K of Si
                                
                FTSeSika, FTSeSikb = PicosDe.EscapeModelado(TRxEscapeSeka, TRxEscapeSekb, self.Ancho,
                                                            ProSeE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTSeSika + FTSeSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchSe=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProSeE)
                SigSe=AnchSe/(2*np.sqrt(2*np.log(2)))

                FSe=np.zeros([len(ProSeE),len(self.E)])
                FTSe=np.zeros(len(self.E))
                
                for i in range(0,len(ProSeE)):
                    FSe[i,:]=round(TkaSeRx[i])*Distribucion.GaussLorentz(self.E, SigSe[i], ProSeE[i])

                    FTSe=FTSe+FSe[i,:]

                self.TkaSeRx = TkaSeRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_SeRx = self.TkaSeRx / self.Real_Time

                ProSeE_Sum, TkaSeRx_Sum = PicosDe.Suma(ProSeE, Tao, Tasa_SeRx)
                                
                FTSeSum = PicosDe.SumaModelado(ProSeE_Sum, self.Ancho, TkaSeRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTSeSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Bromine      ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                CBr=Conc[34]
                rhoBr=3.12 #g/cm3
                
                ProBr=loadtxt('data\ProBr.txt') # Probability that a specific type of X-ray will be generated                       
                ProBrE=ProBr[:,0]
                ProBrRx=ProBr[:,1]

                ProBrQx=loadtxt('data\DatBr.txt') # Energy jump probability and X-ray efficiency

                SBr=ProBrQx[0,:]
                WBr=ProBrQx[1,:]

                ## Rx-K Production ##

                TkaBrRx = Prd.ProductionRxK(13.474, CBr, rhoBr, 
                                ProBr, ProBrQx, DensidadBlanco,
                                self.umaEF[34,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTBrDAu = Dp.ProductionAuL(13.474, CBr, rhoBr,
                                 ProBr, ProBrQx, DensidadBlanco,
                                 self.umaEF[34,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaBrRx = TkaBrRx + FTBrDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProBrE)):
                    TkaBrRx[i] = TkaBrRx[i]*np.exp(-self.CoefAire(ProBrE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProBrE)):
                    TkaBrRx[i] = round(TkaBrRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProBrE[i])*Fact_G[33])

                TRxEscapeBrka, TRxEscapeBrkb = PicosDe.EscapeSi(TkaBrRx, ProBrE, self.E)

                TkaBrRx = TkaBrRx - (TRxEscapeBrka + TRxEscapeBrkb)

                ## X-ray scape for K of Si
                                
                FTBrSika, FTBrSikb = PicosDe.EscapeModelado(TRxEscapeBrka, TRxEscapeBrkb, self.Ancho,
                                                            ProBrE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTBrSika + FTBrSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchBr=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProBrE)
                SigBr=AnchBr/(2*np.sqrt(2*np.log(2)))

                FBr=np.zeros([len(ProBrE),len(self.E)])
                FTBr=np.zeros(len(self.E))
                
                for i in range(0,len(ProBrE)):
                    FBr[i,:]=round(TkaBrRx[i])*Distribucion.GaussLorentz(self.E, SigBr[i], ProBrE[i])

                    FTBr=FTBr+FBr[i,:]

                self.TkaBrRx = TkaBrRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_BrRx = self.TkaBrRx / self.Real_Time

                ProBrE_Sum, TkaBrRx_Sum = PicosDe.Suma(ProBrE, Tao, Tasa_BrRx)
                                
                FTBrSum = PicosDe.SumaModelado(ProBrE_Sum, self.Ancho, TkaBrRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTBrSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Rubidium      ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#


                CRb=Conc[36]
                rhoRb=1.532 #g/cm3
                
                ProRb=loadtxt('data\ProRb.txt') # Probability that a specific type of X-ray will be generated                       
                ProRbE=ProRb[:,0]
                ProRbRx=ProRb[:,1]

                ProRbQx=loadtxt('data\DatRb.txt') # Energy jump probability and X-ray efficiency

                SRb=ProRbQx[0,:]
                WRb=ProRbQx[1,:]

                ## Rx-K Production ##

                TkaRbRx = Prd.ProductionRxK(15.20, CRb, rhoRb, 
                                ProRb, ProRbQx, DensidadBlanco,
                                self.umaEF[36,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTRbDAu = Dp.ProductionAuL(15.20, CRb, rhoRb,
                                 ProRb, ProRbQx, DensidadBlanco,
                                 self.umaEF[36,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaRbRx = TkaRbRx + FTRbDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProRbE)):
                    TkaRbRx[i] = TkaRbRx[i]*np.exp(-self.CoefAire(ProRbE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProRbE)):
                    TkaRbRx[i] = round(TkaRbRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProRbE[i])*Fact_G[35])

                TRxEscapeRbka, TRxEscapeRbkb = PicosDe.EscapeSi(TkaRbRx, ProRbE, self.E)

                TkaRbRx = TkaRbRx - (TRxEscapeRbka + TRxEscapeRbkb)

                ## X-ray scape for K of Si
                                
                FTRbSika, FTRbSikb = PicosDe.EscapeModelado(TRxEscapeRbka, TRxEscapeRbkb, self.Ancho,
                                                            ProRbE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTRbSika + FTRbSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchRb=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProRbE)
                SigRb=AnchRb/(2*np.sqrt(2*np.log(2)))

                FRb=np.zeros([len(ProRbE),len(self.E)])
                FTRb=np.zeros(len(self.E))
                
                for i in range(0,len(ProRbE)):
                    FRb[i,:]=round(TkaRbRx[i])*Distribucion.GaussLorentz(self.E, SigRb[i], ProRbE[i])

                    FTRb=FTRb+FRb[i,:]

                self.TkaRbRx = TkaRbRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_RbRx = self.TkaRbRx / self.Real_Time

                ProRbE_Sum, TkaRbRx_Sum = PicosDe.Suma(ProRbE, Tao, Tasa_RbRx)
                                
                FTRbSum = PicosDe.SumaModelado(ProRbE_Sum, self.Ancho, TkaRbRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTRbSum

                #-----------------------------------------------------------------------------------------------------------------#

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Strontium     ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                CSr=Conc[37]
                rhoSr=2.54 #g/cm3
                
                ProSr=loadtxt('data\ProSr.txt') # Probability that a specific type of X-ray will be generated                       
                ProSrE=ProSr[:,0]
                ProSrRx=ProSr[:,1]

                ProSrQx=loadtxt('data\DatSr.txt') # Energy jump probability and X-ray efficiency

                SSr=ProSrQx[0,:]
                WSr=ProSrQx[1,:]

                ## Rx-K Production ##

                TkaSrRx = Prd.ProductionRxK(16.105, CSr, rhoSr, 
                                ProSr, ProSrQx, DensidadBlanco,
                                self.umaEF[37,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTSrDAu = Dp.ProductionAuL(16.105, CSr, rhoSr,
                                 ProSr, ProSrQx, DensidadBlanco,
                                 self.umaEF[37,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaSrRx = TkaSrRx + FTSrDAu

                ## X-ray attenuation due to air ##

                for i in range(0, len(ProSrE)):
                    TkaSrRx[i] = TkaSrRx[i]*np.exp(-self.CoefAire(ProSrE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProSrE)):
                    TkaSrRx[i] = round(TkaSrRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProSrE[i])*Fact_G[36])

                TRxEscapeSrka, TRxEscapeSrkb = PicosDe.EscapeSi(TkaSrRx, ProSrE, self.E)

                TkaSrRx = TkaSrRx - (TRxEscapeSrka + TRxEscapeSrkb)

                ## X-ray scape for K of Si
                                
                FTSrSika, FTSrSikb = PicosDe.EscapeModelado(TRxEscapeSrka, TRxEscapeSrkb, self.Ancho,
                                                            ProSrE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTSrSika + FTSrSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchSr=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProSrE)
                SigSr=AnchSr/(2*np.sqrt(2*np.log(2)))

                FSr=np.zeros([len(ProSrE),len(self.E)])
                FTSr=np.zeros(len(self.E))
                
                for i in range(0,len(ProSrE)):
                    FSr[i,:]=round(TkaSrRx[i])*Distribucion.GaussLorentz(self.E, SigSr[i], ProSrE[i])

                    FTSr=FTSr+FSr[i,:]

                self.TkaSrRx = TkaSrRx

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in sum

                Tasa_SrRx = self.TkaSrRx / self.Real_Time

                ProSrE_Sum, TkaSrRx_Sum = PicosDe.Suma(ProSrE, Tao, Tasa_SrRx)
                                
                FTSrSum = PicosDe.SumaModelado(ProSrE_Sum, self.Ancho, TkaSrRx_Sum, self.E, self.Real_Time)

                ## Representation

                FTSuma = FTSuma + FTSrSum

                #-----------------------------------------------------------------------------------------------------------------#


                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------          For Yttrium     ----------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                CY=Conc[38]
                rhoY=4.405 #g/cm3
                
                ProY=loadtxt('data\ProY.txt') # Probability that a specific type of X-ray will be generated                       
                ProYE=ProY[:,0]
                ProYRx=ProY[:,1]

                ProYQx=loadtxt('data\DatY.txt') # Energy jump probability and X-ray efficiency

                SY=ProYQx[0,:]
                WY=ProYQx[1,:]


                Posi2=np.zeros(len(ProYE))
                for i in range(0,len(ProYE)):
                    Posi22=np.array(np.where(self.E>ProYE[i]))
                    Posic=np.array(Posi22[0])
                    Posi2[i]=np.array(Posic[0])

                ## For Layer K ##
                Posi_1=np.array(np.where(self.E>17.08)) ## Ionization Potential of Y for the K layer                
                Posici_1=np.array(Posi_1[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the K layer in Y

                ## Rx-K Production ##

                TkaYRx = Prd.ProductionRxK(17.08, CY, rhoY, 
                                ProY[0:5], ProYQx, DensidadBlanco,
                                self.umaEF[38,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTYDAu = Dp.ProductionAuL(17.08, CY, rhoY,
                                 ProY[0:5], ProYQx, DensidadBlanco,
                                 self.umaEF[38,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaYRx = TkaYRx + FTYDAu

                TKlYRx = np.zeros(len(ProYE)-len(TkaYRx))


                ## For Layer L1 ##
                Posi_2=np.array(np.where(self.E>2.373)) ## Ionization Potential of Y for the L1 layer                
                Posici_2=np.array(Posi_2[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L1 layer in Y

                
                for j in range(5,7):
                    for i in range(Posici_2[0],Posici_1[0]):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[1]*(SY[1]-1)/SY[1])*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[1]*(SY[1]-1)/(SY[1]*SY[0]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 2.373): 
                            TKlYRx[j-5]=TKlYRx[j-5]+CY*TAuDRx[i]*(WY[1]*(SY[1]-1)/SY[1])*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                ## For Layer L2 ##
                Posi_3=np.array(np.where(self.E>2.156)) ## Ionization Potential of Y for the L2 layer                
                Posici_3=np.array(Posi_3[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L2 layer in Y

                for j in range(7,10):
                    for i in range(Posici_3[0],Posici_2[0]):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[2]*(SY[2]-1)/SY[2])*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[2]*(SY[2]-1)/(SY[2]*SY[1]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[2]*(SY[2]-1)/(SY[2]*SY[1]*SY[0]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 2.156): 
                            TKlYRx[j-5]=TKlYRx[j-5]+CY*TAuDRx[i]*(WY[2]*(SY[2]-1)/(SY[2]*SY[1]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                ## For Layer L3 ##
                Posi_4=np.array(np.where(self.E>2.08)) ## Ionization Potential of Y for the L3 layer                
                Posici_4=np.array(Posi_4[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L3 layer in Y

                for j in range(10,13):
                    for i in range(Posici_4[0],Posici_3[0]):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[3]*(SY[3]-1)/SY[3])*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_3[0],Posici_2[0]):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[3]*(SY[3]-1)/(SY[3]*SY[2]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[3]*(SY[3]-1)/(SY[3]*SY[2]*SY[1]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TKlYRx[j-5]=TKlYRx[j-5]+CY*Fn[i]*(WY[3]*(SY[3]-1)/(SY[3]*SY[2]*SY[1]*SY[0]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
        

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 2.08): 
                            TKlYRx[j-5]=TKlYRx[j-5]+CY*TAuDRx[i]*(WY[3]*(SY[3]-1)/(SY[3]*SY[2]*SY[1]))*ProYRx[j]*(rhoY/DensidadBlanco)*self.umaEF[38,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                TkaYRx = np.concatenate((TkaYRx, TKlYRx), axis = 0)
                ## X-ray attenuation due to air ##

                for i in range(0, len(ProYE)):
                    TkaYRx[i] = TkaYRx[i]*np.exp(-self.CoefAire(ProYE[i])*DensidadAire*d)
                
                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProYE)):
                    TkaYRx[i] = round(TkaYRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProYE[i])*Fact_G[37])

                TRxEscapeYka, TRxEscapeYkb = PicosDe.EscapeSi(TkaYRx, ProYE, self.E)

                TkaYRx = TkaYRx - (TRxEscapeYka + TRxEscapeYkb)

                ## X-ray scape for K of Si
                                
                FTYSika, FTYSikb = PicosDe.EscapeModelado(TRxEscapeYka, TRxEscapeYkb, self.Ancho,
                                                            ProYE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTYSika + FTYSikb

                #-----------------------------------------------------------------------------------------------------------------#
                
                AnchY=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProYE)
                SigY=AnchY/(2*np.sqrt(2*np.log(2)))

                FY=np.zeros([len(ProYE),len(self.E)])
                FTY=np.zeros(len(self.E))
                
                for i in range(0,len(ProYE)):
                    FY[i,:]=round(TkaYRx[i])*Distribucion.GaussLorentz(self.E, SigY[i], ProYE[i])

                    FTY=FTY+FY[i,:]

                self.TkaYRx = TkaYRx

                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #-----------------------------------------------          For Zirconium     ---------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#
                #------------------------------------------------------------------------------------------------------------------------------#

                CZr=Conc[39]
                rhoZr=6.53 #g/cm3
                
                ProZr=loadtxt('data\ProZr.txt') # Probability that a specific type of X-ray will be generated                       
                ProZrE=ProZr[:,0]
                ProZrRx=ProZr[:,1]

                ProZrQx=loadtxt('data\DatZr.txt') # Energy jump probability and X-ray efficiency

                SZr=ProZrQx[0,:]
                WZr=ProZrQx[1,:]


                Posi2=np.zeros(len(ProZrE))
                for i in range(0,len(ProZrE)):
                    Posi22=np.array(np.where(self.E>ProZrE[i]))
                    Posic=np.array(Posi22[0])
                    Posi2[i]=np.array(Posic[0])

                ## For Layer K ##
                Posi_1=np.array(np.where(self.E>17.9979)) ## Ionization Potential of Zr for the K layer                
                Posici_1=np.array(Posi_1[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the K layer in Zr

                ## Rx-K Production ##

                TkaZrRx = Prd.ProductionRxK(17.9979, CZr, rhoZr, 
                                ProZr[0:5], ProZrQx, DensidadBlanco,
                                self.umaEF[39,:], uT, Fn,
                                self.E, self.Posici1kev[0])
                ## Au L contribution ##
                        
                FTZrDAu = Dp.ProductionAuL(17.9979, CZr, rhoZr,
                                 ProZr[0:5], ProZrQx, DensidadBlanco,
                                 self.umaEF[39,:], uT, ProAuE[5:29],
                                 TAuDRx, self.E, self.Posici1kev[0])


                TkaZrRx = TkaZrRx + FTZrDAu

                TKlZrRx = np.zeros(len(ProZrE)-len(TkaZrRx))

                ## For Layer L1 ##
                Posi_2=np.array(np.where(self.E>2.532)) ## Ionization Potential of Zr for the L1 layer                
                Posici_2=np.array(Posi_2[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L1 layer in Zr

                
                for j in range(5,7):
                    for i in range(Posici_2[0],Posici_1[0]):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[1]*(SZr[1]-1)/SZr[1])*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[1]*(SZr[1]-1)/(SZr[1]*SZr[0]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 2.532): 
                            TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*TAuDRx[i]*(WZr[1]*(SZr[1]-1)/SZr[1])*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                ## For Layer L2 ##
                Posi_3=np.array(np.where(self.E>2.307)) ## Ionization Potential of Zr for the L2 layer                
                Posici_3=np.array(Posi_3[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L2 layer in Zr

                for j in range(7,10):
                    for i in range(Posici_3[0],Posici_2[0]):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[2]*(SZr[2]-1)/SZr[2])*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[2]*(SZr[2]-1)/(SZr[2]*SZr[1]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[2]*(SZr[2]-1)/(SZr[2]*SZr[1]*SZr[0]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 2.307): 
                            TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*TAuDRx[i]*(WZr[2]*(SZr[2]-1)/(SZr[2]*SZr[1]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                ## For Layer L3 ##
                Posi_4=np.array(np.where(self.E>2.223)) ## Ionization Potential of Zr for the L3 layer                
                Posici_4=np.array(Posi_4[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the L3 layer in Zr

                for j in range(10,13):
                    for i in range(Posici_4[0],Posici_3[0]):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[3]*(SZr[3]-1)/SZr[3])*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_3[0],Posici_2[0]):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[3]*(SZr[3]-1)/(SZr[3]*SZr[2]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_2[0],Posici_1[0]):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[3]*(SZr[3]-1)/(SZr[3]*SZr[2]*SZr[1]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])

                    for i in range(Posici_1[0],len(self.E)):
                        TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*Fn[i]*(WZr[3]*(SZr[3]-1)/(SZr[3]*SZr[2]*SZr[1]*SZr[0]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,i-self.Posici1kev[0]]/(uT[i-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])
        

                    #- For Au L -#
                        
                    for i in range(0, len(TAuDRx)):
                        if (self.E[int(PosiAu[i])] > 2.223): 
                            TKlZrRx[j-5]=TKlZrRx[j-5]+CZr*TAuDRx[i]*(WZr[3]*(SZr[3]-1)/(SZr[3]*SZr[2]*SZr[1]))*ProZrRx[j]*(rhoZr/DensidadBlanco)*self.umaEF[39,int(PosiAu[i])-self.Posici1kev[0]]/(uT[int(PosiAu[i])-self.Posici1kev[0]]+uT[int(Posi2[j])-self.Posici1kev[0]])


                TkaZrRx = np.concatenate((TkaZrRx, TKlZrRx), axis = 0)
                ## X-ray attenuation due to air ##

                for i in range(0, len(ProZrE)):
                    TkaZrRx[i] = TkaZrRx[i]*np.exp(-self.CoefAire(ProZrE[i])*DensidadAire*d)

                #-----------------------------------------------------------------------------------------------------------------#
                ## X-ray in scape

                for i in range(0,len(ProZrE)):
                    TkaZrRx[i] = round(TkaZrRx[i]*AngSolBl*Interpolacion.Lineal(self.Energ, self.Efficiency, ProZrE[i])*Fact_G[38])

                TRxEscapeZrka, TRxEscapeZrkb = PicosDe.EscapeSi(TkaZrRx, ProZrE, self.E)

                TkaZrRx = TkaZrRx - (TRxEscapeZrka + TRxEscapeZrkb)

                ## X-ray scape for K of Si
                                
                FTZrSika, FTZrSikb = PicosDe.EscapeModelado(TRxEscapeZrka, TRxEscapeZrkb, self.Ancho,
                                                            ProZrE, self.E)
                
                ## Representation

                FTEscape = FTEscape + FTZrSika + FTZrSikb

                #-----------------------------------------------------------------------------------------------------------------#
              
                AnchZr=Minimos.Cuadrados(self.Ancho[:,0],self.Ancho[:,1],1,ProZrE)
                SigZr=AnchZr/(2*np.sqrt(2*np.log(2)))

                FZr=np.zeros([len(ProZrE),len(self.E)])
                FTZr=np.zeros(len(self.E))
                
                for i in range(0,len(ProZrE)):
                    FZr[i,:]=round(TkaZrRx[i])*Distribucion.GaussLorentz(self.E, SigZr[i], ProZrE[i])

                    FTZr=FTZr+FZr[i,:]

                self.TkaZrRx = TkaZrRx
                
                        
      

                self.Espe = FTEscape + FTSuma + Fc + FTAuD + FTAuDi + FTU +FTBa + FTPb + FTAg + FTSr + FTRb + FTBr + FTZr + FTY + FTSe + FTAs + FTGe + FTGa + FTZn + FTCu + FTNi + FTFe + FTCo + FTMn + FTCr + FTV + FTTi + FTSc + FTCa + FTK + FTCl + FTS + FTP + FTSi + FTAl + FTAr

                
                for i in range(0, len(self.Espe)):
                    self.Espe[i] = round(self.Espe[i])

                self.Mess1 = " Elementos \t \t Concentración [%] \t \t Incertidumbre [%] \t \t Rayos-X Simulados"
                self.Mess2 = "----------- \t \t ------------------ \t \t ------------------ \t \t ------------------------"
                
                ElEnc, ElConc, aste, asterisco = Elementos.Encontrados(lista, Conc)
                RxSimu = np.zeros(len(ElEnc))

                for i in range(0, len(ElEnc)):
                    RxSimu1 = getattr(self, "Tka"+ElEnc[i]+"Rx")
                    try:
                        RxSimu[i] = round(RxSimu1[0] + RxSimu1[1])
                    except:
                        RxSimu[i] = round(RxSimu1[0])


                
                Incer = Elementos.Incertidumbre(ElConc, RxSimu, aste)
                
                self.ElEnc = ElEnc
                self.ElConc = ElConc
                self.aste = aste
                self.asterisco = asterisco
                self.Incer = Incer
                self.RxSimu = RxSimu

                self.Mess3 = ""

                for i in range(0, len(ElEnc)):
                    self.Mess3 =self.Mess3+"    "+ElEnc[i]+aste[i]+" \t \t "+str(round(float(ElConc[i]),5))+" \t \t \t "+str(round(2*float(Incer[i]),5))+" \t \t \t "+str(int(RxSimu[i]))+"\n"
                    
                    #self.Mess3 =self.Mess3+"    "+ElEnc[i]+aste[i]+"\t \t Kα:   "+str(int(RxSimu[i]))+"\t\t"+str(round(float(ElConc[i]),5))+"\t"+str(round(float(Incer[i]),5))+"\n"

                if (asterisco == "no"):
                    self.Mess3 = self.Mess3 +"---------------------------------------------------------------------------------------------------------------------------------------------------------"
                else:                        
                    self.Mess3 = self.Mess3 +"---------------------------------------------------------------------------------------------------------------------------------------------------------"+'\n'+" *:   µg/g \n"
                        
                    
                if self.Lineal.isChecked():
                    self.plt_data2.setLogMode(False, False)

                else:
                    self.plt_data2.setLogMode(False, True)

                
                self.plt_data2.setData(self.E[self.Posici1kev[0]:len(self.E)], self.Espe[self.Posici1kev[0]:len(self.E)], name='Modelling')
                self.Mess4 = "Densidad calculada de la muestra: "+str(round(DensidadBlanco,3))+" g/cm3"
                self.textEdit_1.setText(self.Mess1+"\n"+self.Mess2+"\n"+self.Mess3+"\n"+self.Mess4)
                self.textEdit_1.setStyleSheet("color: white")
                self.ExportBtn.setEnabled(True)


                Chi2 = 0

                for i in range(self.Posici1kev[0], len(self.Espe)):
                    if (self.Espe[i] != 0):
                        Chi2 = Chi2 + (self.ploty[i] - self.Espe[i])**2/self.Espe[i]

                Chi2 = Chi2 / len(self.Espe[self.Posici1kev[0]:len(self.E)])

                self.Mensaje1 = "Valor de Chi-2: "+str(round(Chi2,4))

                Concentration = sum(Conc)*100

                self.Mensaje2 = "Concentración Total [%]: "+str(Concentration)

                if (Concentration > 100):
                    
                    self.Mensaje2 = self.Mensaje2 + "\n (La Concentración no puede superar el 100 %)"
                    self.textEdit_2.setStyleSheet("color: #ff0000")
                    self.textEdit_2.setText('\n'+self.Mensaje1+'\n'+'\n'+self.Mensaje2)

                else:
                    self.textEdit_2.setStyleSheet("color: white")
                    self.textEdit_2.setText('\n'+self.Mensaje1+'\n'+'\n'+self.Mensaje2)


                plt.figure(figsize=(12,5))
                plt.semilogy(self.E[self.Posici1kev[0]:len(self.E)], self.ploty[self.Posici1kev[0]:len(self.E)], color='r', label='Experimental')
                plt.semilogy(self.E[self.Posici1kev[0]:len(self.E)], self.Espe[self.Posici1kev[0]:len(self.E)], color='b', label='Modelado')
                xticks([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30])                
                xlabel("Energía (keV)")
                ylabel("Intensidad (# Cuentas)")
                legend(loc='upper right')
                savefig("grafica.png")

                self.MakeBtn.setEnabled(True)

        except Exception as e:
            print(e)
    
    def CoefAire(self, Energy):
        try:
            Energy = float(Energy)
            datos = np.loadtxt("data\Aire.txt")        
            Val = Interpolacion.Lineal(datos[:,0] * 10 ** 3, datos[:, 4], Energy)
            return Val
            
        except Exception as e:
            print(e)
    
    def GuardarCom(self):

        self.name = QFileDialog.getSaveFileName(None, caption='Select a data file',
                                           filter='Files model x-ray (*.mrx)')

        self.Guar()

    def Guard(self):
        self.name = self.Name
        self.Guar()

    def Guar(self):

        Info = open(self.name[0], 'w')
        Info.write("Información de la modelamiento del espectro"+'\n')

        #---------------- Información de la Calibración ----------------#
        
        Calibra = np.loadtxt("Calibracion.txt")
        Longitud = len(Calibra[:,0])
        
        if Longitud == 2:
            Info.write(str(Calibra[0,0])+" "+str(Calibra[0,1])+'\n')
            Info.write(str(Calibra[1,0])+" "+str(Calibra[1,1])+'\n')
            Info.write(" "+'\n')
            Info.write(" "+'\n')
        elif Longitud == 3:
            Info.write(str(Calibra[0,0])+" "+str(Calibra[0,1])+'\n')
            Info.write(str(Calibra[1,0])+" "+str(Calibra[1,1])+'\n')
            Info.write(str(Calibra[2,0])+" "+str(Calibra[2,1])+'\n')
            Info.write(" "+'\n')
        elif Longitud == 4:
            Info.write(str(Calibra[0,0])+" "+str(Calibra[0,1])+'\n')
            Info.write(str(Calibra[1,0])+" "+str(Calibra[1,1])+'\n')
            Info.write(str(Calibra[2,0])+" "+str(Calibra[2,1])+'\n')
            Info.write(str(Calibra[3,0])+" "+str(Calibra[3,1])+'\n')

        #---------------- Tiempo Real y Tiempo Vivo ----------------#
        Info.write(str(self.Real_Time)+'\n')
        Info.write(str(self.Live_Time)+'\n')
        #---------------- Información del Ancho ----------------#
        Ancho = np.loadtxt("Ancho.txt")
        Longitud = len(Ancho[:,0])
        if Longitud == 2:
            Info.write(str(Ancho[0,0])+" "+str(Ancho[0,1])+'\n')
            Info.write(str(Ancho[1,0])+" "+str(Ancho[1,1])+'\n')
            Info.write(" "+'\n')
            Info.write(" "+'\n')
            Info.write(" "+'\n')
        elif Longitud == 3:
            Info.write(str(Ancho[0,0])+" "+str(Ancho[0,1])+'\n')
            Info.write(str(Ancho[1,0])+" "+str(Ancho[1,1])+'\n')
            Info.write(str(Ancho[2,0])+" "+str(Ancho[2,1])+'\n')
            Info.write(" "+'\n')
            Info.write(" "+'\n')
        elif Longitud == 4:
            Info.write(str(Ancho[0,0])+" "+str(Ancho[0,1])+'\n')
            Info.write(str(Ancho[1,0])+" "+str(Ancho[1,1])+'\n')
            Info.write(str(Ancho[2,0])+" "+str(Ancho[2,1])+'\n')
            Info.write(str(Ancho[3,0])+" "+str(Ancho[3,1])+'\n')
            Info.write(" "+'\n')
        elif Longitud == 5:
            Info.write(str(Ancho[0,0])+" "+str(Ancho[0,1])+'\n')
            Info.write(str(Ancho[1,0])+" "+str(Ancho[1,1])+'\n')
            Info.write(str(Ancho[2,0])+" "+str(Ancho[2,1])+'\n')
            Info.write(str(Ancho[3,0])+" "+str(Ancho[3,1])+'\n')
            Info.write(str(Ancho[4,0])+" "+str(Ancho[4,1])+'\n')
            
        #---------------- Información del Blanco Secundario ----------------#
        
        Blanco = open('BlancoSeleccionado.txt','r')
        Blanco = str(Blanco.readline())
        Info.write(Blanco+'\n')

        Func = open('FuncionBlancoSecundario.txt','r')
        Func = str(Func.readline()).split(';')
        for i in range(0, 9):
            Info.write(Func[i]+'\n')
        
        Func = open('FuncionDistribucion.txt','r')
        Func = str(Func.readline()).split(';')
        for i in range(0, 13):
            Info.write(Func[i]+'\n')
        
        if (self.Convencional.isChecked() == True):
            Info.write("Convencional"+'\n')
        elif (self.Polarizacion.isChecked() == True):
            Info.write("Polarizacion"+'\n')

        if (self.Oxido.isChecked() == True):
            Info.write("Oxido"+'\n')
        elif (self.General.isChecked() == True):
            Info.write("General"+'\n')

        Longitud = len(self.Vec_Datos)

        for i in range(0, Longitud):
            Info.write(str(self.Vec_Datos[i])+'\n')
        
        Conc = np.zeros(98)
        for i in range(1,99):
            try:
                Conc[i-1] = float(getattr(self, "lineEdit_{}".format(i)).text())
            except:
                Conc[i-1] = 0

        for i in range(0, len(Conc) - 1):
            Info.write(str(Conc[i]) + '\n')

        Info.write(str(Conc[len(Conc) - 1]))
        
        Info.close()

        mesa='File saved successfully'
        msg = QMessageBox()
        msg.setStyleSheet(''' background-color: #2f3655;
                              color: white;''')
        msg.setIcon(QMessageBox.Information)
        msg.setText("Well done!")
        msg.setInformativeText(mesa)
        msg.setWindowTitle("Model X-Ray")
        msg.exec_()

    def Insert(self):
        try:
            filename= QFileDialog.getOpenFileName(None, 'Open files', filter='MCA Files (*.mca)',
                                                             options=QFileDialog.Options())
            if filename[0]:                
                with open(filename[0],'r') as DAT_1:

                    for i in range(0, 7):
                        DAT_1.readline()
                    
                    Lin_8=str(DAT_1.readline())
                    Line_8=Lin_8.split('\n')   ##Live Time

                    Live_Time = Line_8[0]

                    Lin_9=str(DAT_1.readline()) 
                    Line_9=Lin_9.split('\n')   ##Real Time

                    Real_Time = Line_9[0]

                    for i in range(0, 4):
                        DAT_1.readline()
            
                    #########################333

                    Lin_14=str(DAT_1.readline())
                    Line_14=Lin_14.split('\n')

                    Lin_15=str(DAT_1.readline())
                    Line_15=Lin_15.split('\n')

                    #########################

                    Lin_16=str(DAT_1.readline())
                    Line_16=Lin_16.split('\n')

                    if str(Line_16[0])=="<<DATA>>":

                        Lin = DAT_1.readlines()
                        self.Vec_Dat = np.zeros(2048)
                        for i in range(0, 2048):
                            self.Vec_Dat[i] = int(Lin[i])

                        Call1 = str(Line_14[0]).split(" ")
                        Call2 = str(Line_15[0]).split(" ")
                        
                        Calibra = open("Calibracion.txt", "w")
                        Calibra.write(str(Call1[0]) + " " + str(Call1[1]) + "\n")
                        Calibra.write(str(Call2[0]) + " " + str(Call2[1]))
                        Calibra.close()

                        self.Calibracion = np.array ([[float(Call1[0]), float(Call1[1])],
                                                     [float(Call2[0]), float(Call2[1])]])
                        
                    else:

                        Lin_17 = str(DAT_1.readline())
                        Line_17 = Lin_17.split('\n')

                        if str(Line_17[0]) == "<<DATA>>":

                            Lin = DAT_1.readlines()                                            
                            self.Vec_Dat = np.zeros(2048)
                            for i in range(0, 2048):
                                self.Vec_Dat[i] = int(Lin[i])

                            Call1 = str(Line_14[0]).split(" ")
                            Call2 = str(Line_15[0]).split(" ")
                            Call3 = str(Line_16[0]).split(" ")
                            
                            Calibra = open("Calibracion.txt", "w")
                            Calibra.write(str(Call1[0]) + " " + str(Call1[1])+"\n")
                            Calibra.write(str(Call2[0]) + " " + str(Call2[1])+"\n")
                            Calibra.write(str(Call3[0]) + " " + str(Call3[1]))
                            Calibra.close()

                            self.Calibracion = np.array([[float(Call1[0]), float(Call1[1])],
                                                         [float(Call2[0]), float(Call2[1])],
                                                         [float(Call3[0]), float(Call3[1])]])
                                                                                  
                        else:
                            Lin_18 = str(DAT_1.readline())
                            Line_18 = Lin_18.split('\n')

                            if str(Line_18[0]) == "<<DATA>>":

                                Lin = DAT_1.readlines()                                            
                                self.Vec_Dat = np.zeros(2048)
                                for i in range(0, 2048):
                                    self.Vec_Dat[i] = int(Lin[i])

                                Call1 = str(Line_14[0]).split(" ")
                                Call2 = str(Line_15[0]).split(" ")
                                Call3 = str(Line_16[0]).split(" ")
                                Call4 = str(Line_17[0]).split(" ")
                                
                                Calibra = open("Calibracion.txt", "w")
                                Calibra.write(str(Call1[0]) + " " + str(Call1[1]) + "\n")
                                Calibra.write(str(Call2[0]) + " " + str(Call2[1]) + "\n")
                                Calibra.write(str(Call3[0]) + " " + str(Call3[1]) + "\n")
                                Calibra.write(str(Call4[0]) + " " + str(Call4[1]))
                                Calibra.close()

                                self.Calibracion = np.array([[float(Call1[0]), float(Call1[1])],
                                                            [float(Call2[0]), float(Call2[1])],
                                                            [float(Call3[0]), float(Call3[1])],
                                                            [float(Call4[0]), float(Call4[1])]])
                    
                    np.savetxt("ExpDat.txt", self.Vec_Dat)
                    
                    
            self.Real_Time = float(Real_Time.split(" - ")[1])
            self.Live_Time = float(Live_Time.split(" - ")[1])
        
            cantiCana = 2048 
            
            x = np.array(list(range(0, cantiCana))) # Eje X
            
            
            self.E = Minimos.Cuadrados(self.Calibracion[:,0], self.Calibracion[:,1], 1, x)
            self.plotx = self.E
            self.ploty = self.Vec_Dat
            self.Vec_Datos = self.Vec_Dat
            if self.Lineal.isChecked():
                self.plt_data.setLogMode(False, False)
            else:
                self.plt_data.setLogMode(False, True)
            self.plt_data.setData(self.plotx[self.Posici1kev[0]:len(self.E)], self.ploty[self.Posici1kev[0]:len(self.E)], name='Experimental')
            self.textEdit_1.setText("Espectro Experimental: "+filename[0])
            self.textEdit_1.setStyleSheet("color: #ff0000")

            self.NombreExpe = filename[0]
            
        except Exception as e:
            print(e)
    
    def run(self):
        try:
            self.event = "Nothing"
            self.event = Event()
            self.t = threading.Thread(target = self.CalcuCoef)
            self.t.start()
        except Exception as e:                        
            print(e)
            pass

    def CalcuCoef(self):
        try:
            event = self.event 
            self.Calibracion = np.loadtxt("Calibracion.txt")
            cantiCana = 2049
            x = np.array(list(range(1,cantiCana))) # Eje X   
            self.E = Minimos.Cuadrados(self.Calibracion[:,0],self.Calibracion[:,1],1,x) ## El numero "1" corresponde al orden del ajuste por mínimos cuadrados
            lista = ['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al', 'Si', 'P', 'S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu',
                     'Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La',
                     'Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Po','At',
                     'Rn','Fr','Ra','Ac','Th','Pa','U','Np','Pu','Am','Cm','Bk','Cf']
            
            Posi = np.array(np.where(self.E > 1)) ## Para interpolación efectiva (debido a que se dispone de datos de los coef. posteriores a 1 keV)
            self.Posici1kev = np.array(Posi[0]) ## Posición posterior a 1 keV

            
            self.umaEF = np.zeros([len(lista),len(self.E[self.Posici1kev[0]:len(self.E)])])
            self.umaD = np.zeros([len(lista),len(self.E[self.Posici1kev[0]:len(self.E)])])
            self.umaI = np.zeros([len(lista),len(self.E[self.Posici1kev[0]:len(self.E)])])
            self.umaT = np.zeros([len(lista),len(self.E[self.Posici1kev[0]:len(self.E)])])
            
            for i in range(0, len(lista)):
                datos = np.loadtxt('data\\'+lista[i]+".txt")
                for j in range(self.Posici1kev[0], len(self.E)):                 
                    self.umaD[i,j-self.Posici1kev[0]] = Interpolacion.Lineal(datos[:,0]*10**3,datos[:,1],self.E[j])
                    self.umaI[i,j-self.Posici1kev[0]] = Interpolacion.Lineal(datos[:,0]*10**3,datos[:,2],self.E[j])
                    self.umaEF[i,j-self.Posici1kev[0]] = Interpolacion.Lineal(datos[:,0]*10**3,datos[:,3],self.E[j])
                    self.umaT[i,j-self.Posici1kev[0]] = Interpolacion.Lineal(datos[:,0]*10**3,datos[:,4],self.E[j])
            
            self.event.set()
        except:
            pass

    def AgrandarMenuLateral(self):

        if True:
            width = self.leftMenuContenedor.width()
            Tamano = self.widget.width()
            norm = 70
            if width == 70:
                exten = Tamano / 2 - 7
            else:
                exten = norm

            self.ani = QPropertyAnimation(self.leftMenuContenedor, b'minimumWidth')
            self.ani.setDuration(300)
            self.ani.setStartValue(width)
            self.ani.setEndValue(exten)
            self.ani.setEasingCurve(QtCore.QEasingCurve.InOutQuart)
            self.ani.start()
    
    def mouseMoved(self, evt):
        if self.plt_wid.sceneBoundingRect().contains(evt):
            mousePoint = self.plt_wid.vb.mapSceneToView(evt)
            self.cursorlabel.setText(
                    "<span style='font-size: 10pt; color: #294760'>E (keV) = {:0.2f}, \
                     <span style='color: #294760'>I = {:0.3f}</span>".format(
                         mousePoint.x(), mousePoint.y())
                    )

    def Logaritmo(self):
        try:
            self.Logaritmico.setChecked(True)
            self.Lineal.setChecked(False)
            try:            
                self.plt_data.setLogMode(False, True)
            except:
                pass
            try:
                self.plt_data2.setLogMode(False, True)            
            except:
                pass
        except:
            pass

    def Linea(self):
        try:
            self.Lineal.setChecked(True)
            self.Logaritmico.setChecked(False)
            try:            
                self.plt_data.setLogMode(False, False)
            except:
                pass
            try:
                self.plt_data2.setLogMode(False, False) 
            except:
                pass
        except:
            pass

    def Oxid(self):
        try:
            self.lineEdit_8.setEnabled(False)
            self.General.setChecked(False)
            self.Oxido.setChecked(True)
            
        except Exception as e:
            print(e)

    def Genera(self):
        try:
            self.lineEdit_8.setEnabled(True)
            self.Oxido.setChecked(False)
            self.General.setChecked(True)            
        except:
            pass

    def Convenciona(self):
        try:            
            self.Convencional.setChecked(True)
        except:
            pass

    def Distribucio(self):
        try:            
            Ui_Distribucion().exec_() 
        except Exception as e:
            print(e)

    def AnchoMediaAltura(self):
        try:            
            Ui_AnchoMediaAltura().exec_() 
        except Exception as e:
            print(e)

    def Calibracio(self):
        try:            
            Ui_Calibracion().exec_() 
        except Exception as e:
            print(e)

    def AboutTheSoft(self):
        try:            
            Ui_Acerca().exec_() 
        except Exception as e:
            print(e)

class Ui_Distribucion(QDialog):
    def __init__(self):
        super(Ui_Distribucion, self).__init__(None)
        
        uic.loadUi("IGdistribution.ui", self)
        self.setWindowIcon(QIcon('images\\0.ico'))

        self.pushButton.clicked.connect(self.Aceptar)

        try:
            Fun = open('FuncionDistribucion.txt','r')
            FunD = str(Fun.readline()).split(';')

            for i in range(0, len(FunD)):
                getattr(self, "lineEdit_{}".format(i + 1)).setText(FunD[i])

        except Exception as e:
            print(e)
            pass

    def Aceptar(self):

        try:
            Variable = ""            
            for i in range(0, 12):
                Value = float(getattr(self, "lineEdit_{}".format(i + 1)).text())
                Variable = Variable + str(Value) + ";"

            Ecol = float(self.lineEdit_13.text())

            Variable = Variable + str(Ecol)
            
            FunD = open('FuncionDistribucion.txt','w')
            FunD.write(Variable)
            FunD.close()

            for i in range(0, 13):
                Value = getattr(self, "lineEdit_{}".format(i + 1)).setText("")


            mesa='Successfully Registered'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')
            msg.setIcon(QMessageBox.Information)
            msg.setText("Energy Distribution Function:")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
            
            self.close()

            #.... Send signal to execute Modelling

        except Exception as e:
            print(e)
            mesa='Insert only numbers'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Energy Distribution Function:")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
   
class Ui_AnchoMediaAltura(QDialog):
    def __init__(self):
        super(Ui_AnchoMediaAltura, self).__init__(None)

        uic.loadUi("IGfwhm.ui", self)
        self.setWindowIcon(QIcon('images\\0.ico'))

        try:
            self.Ancho=np.loadtxt("Ancho.txt")

            self.lineEdit.setText(str(self.Ancho[0,0]))
            self.lineEdit_x.setText(str(self.Ancho[0,1]))
            

            self.lineEdit_2.setText(str(self.Ancho[1,0]))
            self.lineEdit_2_x.setText(str(self.Ancho[1,1]))

        except:
            mesa='Insufficient data'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')
            msg.setIcon(QMessageBox.Critical)
            msg.setText("Width at Half Maximum Adjustment:")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
            

        try:
            self.lineEdit_3.setText(str(self.Ancho[2,0]))
            self.lineEdit_3_x.setText(str(self.Ancho[2,1]))
        except:
            pass

        try:
            self.lineEdit_4.setText(str(self.Ancho[3,0]))
            self.lineEdit_4_x.setText(str(self.Ancho[3,1]))
        except:
            pass

        try:
            self.lineEdit_5.setText(str(self.Ancho[4,0]))
            self.lineEdit_5_x.setText(str(self.Ancho[4,1]))
        except:
            pass

        self.pushButton.clicked.connect(self.Registra)

    def Registra(self):

        try:
            C1=float(self.lineEdit.text())
            P1=float(self.lineEdit_x.text())

            C2=float(self.lineEdit_2.text())
            P2=float(self.lineEdit_2_x.text())

            Anch= np.array([[C1, P1],
                            [C2, P2]])
            
            np.savetxt("Ancho.txt", Anch)

            try:
                C3=float(self.lineEdit_3.text())
                P3=float(self.lineEdit_3_x.text())

                Anch= np.array([[C1, P1],
                                [C2, P2],
                                [C3, P3]])
            
                np.savetxt("Ancho.txt", Anch)
            except:
                pass

            try:
                C4=float(self.lineEdit_4.text())
                P4=float(self.lineEdit_4_x.text())

                Anch= np.array([[C1, P1],
                                [C2, P2],
                                [C3, P3],
                                [C4, P4]])
            
                np.savetxt("Ancho.txt", Anch)
            except:
                pass

            try:
                C5=float(self.lineEdit_5.text())
                P5=float(self.lineEdit_5_x.text())

                Anch= np.array([[C1, P1],
                                [C2, P2],
                                [C3, P3],
                                [C4, P4],
                                [C5, P5]])
            
                np.savetxt("Ancho.txt", Anch)
            except:
                pass
            
            mesa='Successfully Registered'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')
            msg.setIcon(QMessageBox.Information)
            msg.setText("Width at Half Maximum Adjustment:")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()

            self.close()

        except Exception as e:
            print(e)
   
class Ui_Calibracion(QDialog):
    def __init__(self):
        super(Ui_Calibracion, self).__init__(None)

        uic.loadUi("IGcalibration.ui", self)
        self.setWindowIcon(QIcon('images\\0.ico'))

        try:
            self.Calibracion=np.loadtxt("Calibracion.txt")

            self.lineEdit.setText(str(self.Calibracion[0,0]))
            self.lineEdit_x.setText(str(self.Calibracion[0,1]))

            self.lineEdit_2.setText(str(self.Calibracion[1,0]))
            self.lineEdit_2_x.setText(str(self.Calibracion[1,1]))

        except:
            mesa = 'Insufficient data'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')

            msg.setIcon(QMessageBox.Critical)
            msg.setText("Calibration adjustment")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()
            

        try:
            self.lineEdit_3.setText(str(self.Calibracion[2,0]))
            self.lineEdit_3_x.setText(str(self.Calibracion[2,1]))
        except:
            pass

        try:
            self.lineEdit_4.setText(str(self.Calibracion[3,0]))
            self.lineEdit_4_x.setText(str(self.Calibracion[3,1]))
        except:
            pass

        self.pushButton.clicked.connect(self.Registra)

    def Registra(self):

        try:
            C1=float(self.lineEdit.text())
            P1=float(self.lineEdit_x.text())

            C2=float(self.lineEdit_2.text())
            P2=float(self.lineEdit_2_x.text())

            Calibrac= np.array([[C1, P1],
                               [C2, P2]])            

            np.savetxt("Calibracion.txt", Calibrac)

            try:

                C3=float(self.lineEdit_3.text())
                P3=float(self.lineEdit_3_x.text())
                Calibrac= np.array([[C1, P1],
                                   [C2, P2],
                                   [C3, P3]])
            
                np.savetxt("Calibracion.txt", Calibrac)

            except:
                pass

            try:

                C4=float(self.lineEdit_4.text())
                P4=float(self.lineEdit_4_x.text())
                Calibrac= np.array([[C1, P1],
                                   [C2, P2],
                                   [C3, P3],
                                   [C4, P4]])
            
                np.savetxt("Calibracion.txt", Calibrac)

            except:
                pass

            mesa='Successfully Registered'
            msg = QMessageBox()
            msg.setStyleSheet(''' background-color: #2f3655;
                                  color: white;''')
            msg.setIcon(QMessageBox.Information)
            msg.setText("Calibration Adjustment")
            msg.setInformativeText(mesa)
            msg.setWindowTitle("Model X-Ray")
            msg.exec_()

            self.close()

        except Exception as e:
            print(e)

class Ui_Acerca(QDialog):    
    def __init__(self):
        super(Ui_Acerca, self).__init__(None)                
        uic.loadUi("IGabout.ui", self)
        self.setWindowIcon(QIcon('images\\0.ico'))

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)    
    app.setWindowIcon(QtGui.QIcon('images\\0.ico'))
    window = Ui_Main()
    window.show()
    sys.exit(app.exec_())  
