import numpy as np

def Lineal(X, Y, x):
    X = np.array(X)
    Y = np.array(Y)
    x = float(x)

    filas = len(X)
    try:
        for i in range(0, filas):
            if (x >= X[i] and x <= X[i+1]):
                y = Y[i] + ((Y[i + 1] - Y[i]) / (X[i + 1]-X[i])) * (x - X[i])
    except Exception as e:
        print(e)
        
    return y

def LinealVarios(X, Y, Z, W, x):
    X = np.array(X)
    Y = np.array(Y)
    Z = np.array(Z)
    W = np.array(W)
    x = float(x)

    filas = len(X)
    try:
        for i in range(0, filas):
            if (x >= X[i] and x <= X[i+1]):
                y = Y[i] + ((Y[i + 1] - Y[i]) / (X[i + 1] - X[i])) * (x - X[i])
                z = Z[i] + ((Z[i + 1] - Z[i]) / (X[i + 1] - X[i])) * (x - X[i])
                w = W[i] + ((W[i + 1] - W[i]) / (X[i + 1] - X[i])) * (x - X[i])
    except:
        pass
    r = [y, z, w]
        
    return r
