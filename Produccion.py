import numpy as np

def ProductionRxK(Ek, CElement, rhoElement,
                  ProElement, ProElementQx,
                  rhoSample, umaEF1, uT1,
                  Fn, E, Posicion1kev):
    
    Ek = float(Ek)
    CElement = float(CElement)
    rhoElement = float(rhoElement)
    rhoSample = float(rhoSample)
    umaEF1 =np.array(umaEF1)
    uT1 = np.array(uT1)
    Posicion1kev = int(Posicion1kev)
    Fn = np.array(Fn)
    E = np.array(E)

    ProElementE = ProElement[:,0]
    ProElementRx = ProElement[:,1]
    
    SElement = ProElementQx[0,:]
    WElement = ProElementQx[1,:]

    TkaElemRx = np.zeros(len(ProElementE))

    Posi2=np.zeros(len(ProElementE))
    for i in range(0,len(ProElementE)):
        Posi22=np.array(np.where(E>ProElementE[i]))
        Posic=np.array(Posi22[0])
        Posi2[i]=np.array(Posic[0])


    ## For Layer K ##
    Posi=np.array(np.where(E>Ek)) ## Ionization Potential of Element for the K layer
    Posici=np.array(Posi[0]) #Posi[0] indicates the channel after the energy of the ionization potential of the K layer in the Element
    
    for j in range(0,len(Posi2)):
        for i in range(Posici[0],len(E)):
            TkaElemRx[j]=TkaElemRx[j]+CElement*Fn[i]*(WElement[0]*(SElement[0]-1)/SElement[0])*ProElementRx[j]*(rhoElement/rhoSample)*umaEF1[i-Posicion1kev]/(uT1[i-Posicion1kev]+uT1[int(Posi2[j])-Posicion1kev])


    return TkaElemRx











        
