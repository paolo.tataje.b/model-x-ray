import numpy as np

def Cuadrados(X, Y, numexpo, x):
    X = np.array(X) #------------------- Eje x 
    Y = np.array(Y) #------------------- Eje y
    x = np.array(x) #------------------- Punto "x" evaluado
    numexpo = int(numexpo) #------------ Exponente de la ec. interpolada
    sigma = np.ones(len(Y)) #----------- Ponderación
    YProm = sum(Y) / len(Y)
    w = 1 / sigma
    g = w
    St = 0 #---------------------------- Cuadrado de los residuos
    Sr = 0 #---------------------------- Cuadrado de las diferencias
    for i in range(0, len(Y)):
        St = St + (Y[i] - YProm)**2 #--- Suma del cuadrado de las diferencias

    Bcal = np.array([np.ones(len(X)) * g] + [(X ** n) * g for n in range(1, numexpo + 1)]).T
    
    BtB = np.dot(np.transpose(Bcal), Bcal)
    BtQ = np.dot(np.transpose(Bcal), Y * g)
    a = np.dot(np.linalg.inv(BtB), BtQ)

    func = a[0]
    for n in range(1, numexpo + 1):
        func += a[n] * x ** n
        
    try:
        func1 = a[0]
        for n in range(1, numexpo+1):
            func1 += a[n] * X ** n
            
        Sr = sum(( Y - (func1))**2)
        Syx = Sr / (len(Y) - 2)
        R = (St - Sr) / St
        
        cov = np.linalg.inv(BtB) * Syx        
    except:
        pass
    
    return func


        
     
    
    
